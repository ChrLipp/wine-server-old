import {deserialize} from 'cerialize';
import {MinLength} from 'class-validator';

import {IStorageSpaceDocument} from '../model/storage.space.document';

/**
 * Input Data for creating or updateing a storage space.
 * Important: the structure of the input and the names of the attributes
 * MUST be ident to the document (StorageSpaceDocument).
 */
export class StorageSpaceRequest implements IStorageSpaceDocument {
    /** UserId for the owner of the storage space */
    public userId: string;

    /**
     * The name of the storage space. The user is responsible to setup a possible hierarchie
     * via the name, e.g.
     * - row 1, column 1
     * - ...
     * - row 4, column 4
     */
    @deserialize
    @MinLength(3)
    public name: string;
}
