import {inject, injectable} from 'inversify';
import {MongoError} from 'mongodb';

import {IStorageSpaceDocument} from '../model/storage.space.document';
import {StorageSpaceRequest} from '../request/storage.space.request';
import {StorageSpaceRepository} from '../repository/storage.space.repository';
import {TYPES} from '../../../ioc/types';
import {ValidationException} from '../../../core/error/exception/validation.exception';

/**
 * Storage space service.
 */
@injectable()
export class StorageSpaceService {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(
        @inject(TYPES.StorageSpaceRepository)
        private storageSpaceRepository: StorageSpaceRepository) {
    }

    /**
     * Creates a storage space.
     * @param storageSpace the storage space
     * @returns {Promise<any>} the created storage space
     */
    public async createStorageSpace(storageSpaceRequest: StorageSpaceRequest):
    Promise<IStorageSpaceDocument> {
        try {
            return await this.storageSpaceRepository.create(storageSpaceRequest);
        }
        catch (error) {
            if (error instanceof MongoError) {
                throw new ValidationException(
                    'A storage space with the same name already exists.');
            }
            throw error;
        }
    }

    /**
     * Returns all storage spaces.
     * @param userId            UserId of the current user
     * @returns {Promise<IStorageSpaceDocument[]>} list of all storage spaces
     */
    public async getStorageSpaces(userId: string): Promise<any> {
        return this.storageSpaceRepository.find({userId: userId});
    }

    /**
     * Retrieves the storage space with the given id(s).
     * The storage space id would be enough to identificate the storage space,
     * but the check for the user id blocks enumerations of the id.
     * @param userId            UserId of the current user
     * @param storageSpaceId    ID of the storage space to retrieve
     * @returns {Promise<any>}
     */
    public async getStorageSpace(userId: string, storageSpaceId: string):
    Promise<IStorageSpaceDocument> {
        return this.storageSpaceRepository.find({_id: storageSpaceId, userId: userId});
    }

    /**
     * Updates a storage space with the given id.
     * @param id            ID of the storage space to update
     * @param storageSpace  the storage space
     * @returns {Promise<number>} count of affected items (1 oder 0)
     */
    public async updateStorageSpace(id: string, storageSpaceRequest: StorageSpaceRequest):
    Promise<void> {
        return this.storageSpaceRepository.update(id, storageSpaceRequest);
    }

    /**
     * Deletes a storage space with the given id.
     * @param id    ID of the storage space to update
     * @returns {Promise<void>}
     */
    public async deleteStorageSpace(id: string): Promise<void> {
        return this.storageSpaceRepository.delete(id);
    }
}
