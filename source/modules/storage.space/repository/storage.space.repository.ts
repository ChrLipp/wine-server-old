import {injectable, inject} from 'inversify';

import {BaseRepository} from '../../../core/base/base.repository';
import {Database} from '../../../core/database/database';
import {IStorageSpaceDocument, StorageSpaceDocument} from '../model/storage.space.document';
import {TYPES} from '../../../ioc/types';

/**
 * Storage space repository.
 * The repository has to initialise the model.
 */
@injectable()
export class StorageSpaceRepository
extends BaseRepository<IStorageSpaceDocument, StorageSpaceDocument> {
    /**
     * Ctor.
     * @param database Database singleton
     */
    constructor(@inject(TYPES.Database) database: Database) {
        super();
        this.model = database.storageSpaces;
    }
}
