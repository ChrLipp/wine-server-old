import 'reflect-metadata';
import * as chai from 'chai';
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import {expect} from 'chai';
import {Request} from 'express';

import {StorageSpaceController} from '../controller/storage.space.controller';
import {StorageSpaceService} from '../service/storage.space.service';
import {ValidationException} from '../../../core/error/exception/validation.exception';

chai.use(sinonChai);

describe('StorageSpace controller', () => {
    let controller: StorageSpaceController;
    let serviceStub: any;

    before(() => {
        serviceStub  = <any> sinon.createStubInstance(StorageSpaceService);
        controller = new StorageSpaceController(<StorageSpaceService> <any> serviceStub);
    });

    describe('getStorageSpace', () => {
        it('throws a ValidationException when invalid ID is given', () => {
            const request = {
                params: {
                    id:         '1'
                },
                user: {
                    userId:     '5'
                }
            };
            return controller
                .getStorageSpace(<Request> request)
                .catch((e) => {
                    expect(e instanceof ValidationException).to.be.true;
                    expect(serviceStub.getStorageSpace).to.have.not.been.called;
                });
        });

        it('calls the service layer and maps the request', async () => {
            const expectedValue = {
                _id:            '581a1745571e150010ae2872',
                currentAmount:  0,
                lastInventory:  '2016-11-02T16:41:41.921Z',
                maximalAmount:  0,
                name:           'Test',
                remove:         42
            };
            serviceStub.getStorageSpace.returns(expectedValue);

            const request = {
                params: {
                    id:         '581a1745571e150010ae2872'
                },
                user: {
                    userId:     '5'
                }
            };
            const returnValue = await controller.getStorageSpace(<Request> request);

            // service call
            expect(serviceStub.getStorageSpace).to.have.been.called;

            // mapped Attibute
            expect(returnValue.id).to.equal(expectedValue._id);

            // 1:1 mapping
            expect(returnValue.currentAmount).to.equal(expectedValue.currentAmount);
            expect(returnValue.maximalAmount).to.equal(expectedValue.maximalAmount);
            expect(returnValue.name).to.equal(expectedValue.name);
            expect(returnValue.lastInventory).to.equal(expectedValue.lastInventory);

            // removed attribute
            expect(returnValue.remove).to.be.undefined;
        });
    });
});
