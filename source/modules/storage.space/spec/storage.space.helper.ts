import {Database} from '../../../core/database/database';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';
import {StorageSpaceDocument, IStorageSpaceDocument} from '../model/storage.space.document';
import {UserDocument} from '../../user/model/user.document';

/**
 * Helper methods for testing the storage space.
 */
export class StorageSpaceHelper {
    /** Database for direct MongoDB manipulation */
    private database: Database;

    /** Test storage space data for tests. This name MUST NOT be used in production. */
    public readonly testStorageSpace: string = 'Test-Storage 4x(z';

    /**
     * Ctor.
     */
    public constructor() {
        this.database = <Database> container.get(TYPES.Database);
    }

    /**
     * Returns the storage space from MongoDB.
     * @returns {Promise<null|StorageSpaceDocument>}
     */
    public async getStorageSpace(): Promise<StorageSpaceDocument | null> {
        return await this.database.storageSpaces.findOne({'name': this.testStorageSpace});
    }

    /**
     * Returns the storage space with the given id from MongoDB.
     * @param id    Given id
     * @returns {Promise<null|StorageSpaceDocument>}
     */
    public async getStorageSpaceById(id: string): Promise<StorageSpaceDocument | null> {
        return await this.database.storageSpaces.findOne(id);
    }

    /**
     * Deletes all storage spaces for the given user from MongoDB.
     * @param user  Given user
     * @returns {Promise<void>}
     */
    public async removeStorageSpace(user: UserDocument): Promise<void> {
        await this.database.storageSpaces.remove({userId: user._id});
    }

    /**
     * Saves a storage space.
     * @param storageSpace  Storage space to save
     * @returns {Promise<Bluebird<StorageSpaceDocument>>}
     */
    public async saveStorageSpace(storageSpace: IStorageSpaceDocument):
    Promise<StorageSpaceDocument> {
        return this.database.storageSpaces.insert(storageSpace);
    }

    /**
     * Creates a storage space for the given user with hardcoded data.
     * @param user  Given user
     * @returns {Promise<StorageSpaceDocument>}
     */
    public async createStorageSpace(user: UserDocument) {
        const userDocument: IStorageSpaceDocument = {
            name:       this.testStorageSpace,
            userId:     user._id
        };
        return await this.saveStorageSpace(userDocument);
    }
}
