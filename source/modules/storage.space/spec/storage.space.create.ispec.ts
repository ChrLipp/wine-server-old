/* tslint:disable:no-var-requires */
const request = require('supertest-as-promised');
/* tslint:enable:no-var-requires */
import {expect} from 'chai';

import {Application} from '../../../application';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';
import {UserDocument} from '../../user/model/user.document';
import {UserHelper} from '../../user/spec/user.helper';
import {StorageSpaceHelper} from './storage.space.helper';

describe('StorageSpace api - create:', function() {
    let app: Application;

    const userHelper = new UserHelper();
    const storageSpaceHelper = new StorageSpaceHelper();

    let user: UserDocument;
    const storageSpaceRequest = {
        name: storageSpaceHelper.testStorageSpace
    };

    before(async function(): Promise<void> {
        app = <Application> container.get(TYPES.Application);
        await app.init();
        await userHelper.removeUser();
        user = await userHelper.createUser();
    });

    beforeEach(async function() {
        await storageSpaceHelper.removeStorageSpace(user);
    });

    it('Denies a creation when user is not logged in', async () => {
        await request(app.getExpressServer())
            .post(`/api/storage`)
            .send(storageSpaceRequest)
            .set('Accept', 'application/json')
            .expect(401);
    });

    it('Creates a storage space', async () => {
        const loginResponse = await userHelper.login(app);
        const storageSpaceResponse = await request(app.getExpressServer())
            .post(`/api/storage`)
            .send(storageSpaceRequest)
            .set('Accept', 'application/json')
            .set('Authorization', `JWT ${loginResponse.header.jwt}`)
            .expect(201);
        expect(storageSpaceResponse.body.name).to.be.equal(storageSpaceHelper.testStorageSpace);
        expect(storageSpaceResponse.body.currentAmount).to.be.equal(0);
        expect(storageSpaceResponse.body.maximalAmount).to.be.equal(0);

        const storageSpaceDoc = await storageSpaceHelper.getStorageSpace();
        expect(storageSpaceDoc).to.not.be.null;
        if (storageSpaceDoc) {
            expect(storageSpaceDoc.name).to.be.equal(storageSpaceHelper.testStorageSpace);
            expect(storageSpaceDoc.currentAmount).to.be.equal(0);
            expect(storageSpaceDoc.maximalAmount).to.be.equal(0);
            expect(storageSpaceDoc.userId).to.be.equal(user._id);
        }
    });

    it('Fails when a storage space with the same name is already existing', async () => {
        await storageSpaceHelper.createStorageSpace(user);
        const loginResponse = await userHelper.login(app);
        await request(app.getExpressServer())
            .post(`/api/storage`)
            .send(storageSpaceRequest)
            .set('Accept', 'application/json')
            .set('Authorization', `JWT ${loginResponse.header.jwt}`)
            .expect(400);
    });

    afterEach(async function() {
        await storageSpaceHelper.removeStorageSpace(user);
    });

    after(async function() {
        await userHelper.removeUser();
        return app.prepareShutdown();
    });
});
