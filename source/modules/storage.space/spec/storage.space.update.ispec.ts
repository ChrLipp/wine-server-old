/* tslint:disable:no-var-requires */
const request = require('supertest-as-promised');
/* tslint:enable:no-var-requires */
import {expect} from 'chai';

import {Application} from '../../../application';
import container from '../../../ioc/bootstrap';
import {StorageSpaceDocument} from '../model/storage.space.document';
import {StorageSpaceHelper} from './storage.space.helper';
import {TYPES} from '../../../ioc/types';
import {UserDocument} from '../../user/model/user.document';
import {UserHelper} from '../../user/spec/user.helper';

describe('StorageSpace api - update:', function() {
    let app: Application;

    const userHelper = new UserHelper();
    const storageSpaceHelper = new StorageSpaceHelper();

    let user: UserDocument;
    let storageSpace: StorageSpaceDocument;
    const updatedName = 'Updated';

    let storageSpaceRequest = {
        name: updatedName
    };

    before(async function(): Promise<void> {
        app = <Application> container.get(TYPES.Application);
        await app.init();
        await userHelper.removeUser();
        user = await userHelper.createUser();
    });

    beforeEach(async function() {
        await storageSpaceHelper.removeStorageSpace(user);
        storageSpace = await storageSpaceHelper.createStorageSpace(user);
    });

    it('Denies an update when user is not logged in', async () => {
        await request(app.getExpressServer())
            .put(`/api/storage/${user._id}`)
            .send(storageSpaceRequest)
            .set('Accept', 'application/json')
            .expect(401);
    });

    it('Updates a storage space', async () => {
        const loginResponse = await userHelper.login(app);
        await request(app.getExpressServer())
            .put(`/api/storage/${storageSpace._id}`)
            .send(storageSpaceRequest)
            .set('Accept', 'application/json')
            .set('Authorization', `JWT ${loginResponse.header.jwt}`)
            .expect(200);

        const storageSpaceDoc = await storageSpaceHelper.getStorageSpaceById(storageSpace._id);
        expect(storageSpaceDoc).to.not.be.null;
        if (storageSpaceDoc) {
            expect(storageSpaceDoc.name).to.be.equal(updatedName);
            expect(storageSpaceDoc.currentAmount).to.be.equal(0);
            expect(storageSpaceDoc.maximalAmount).to.be.equal(0);
            expect(storageSpaceDoc.userId).to.be.equal(user._id);
        }
    });

    afterEach(async function() {
        await storageSpaceHelper.removeStorageSpace(user);
    });

    after(async function() {
        await userHelper.removeUser();
        return app.prepareShutdown();
    });
});
