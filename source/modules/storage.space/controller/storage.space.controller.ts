import * as passport from 'passport';

import {Serialize} from 'cerialize';
import {injectable, inject} from 'inversify';
import {Controller, Delete, Get, Post, Put} from 'inversify-express-utils';
import {Request, Response} from 'express';

import {iridiumCursor2JsonResponse} from '../../../core/util/controller.transform';
import {validateBody, validateMongoId} from '../../../core/util/controller.validate';
import {StorageSpaceDocument} from '../model/storage.space.document';
import {StorageSpaceRequest} from '../request/storage.space.request';
import {StorageSpaceService} from '../service/storage.space.service';
import {TYPES} from '../../../ioc/types';

/**
 * Storage space controller.
 */
@Controller('/api/storage')
@injectable()
export class StorageSpaceController {
    /**
     * Ctor.
     * @param storageSpaceService The StorageSpace service
     */
    constructor(
        @inject(TYPES.StorageSpaceService) private storageSpaceService: StorageSpaceService) {
    }

    /**
     * @apiGroup StorageSpace
     * @apiName getStorageSpaces
     * @api {get} /api/storage List storage spaces
     * @apiPermission standard
     */
    /**
     * Returns all persisted storage spaces.
     * @returns {Promise<IStorageSpaceDocument[]>} all persisted storage spaces
     */
    @Get('/', passport.authenticate('jwt', {session: false}))
    public async getStorageSpaces(request: Request, response: Response): Promise<void> {
        const cursor = await this.storageSpaceService.getStorageSpaces(request.user.userId);
        iridiumCursor2JsonResponse(cursor, response, StorageSpaceDocument);
    }

    /**
     * @apiGroup storage space
     * @apiName getStorageSpace
     * @api {get} /api/storage/:id Returns the storage space with the given id
     */
    /**
     * Returns the persisted storage space with the given id.
     * @param   request id
     * @returns {Promise<IStorageSpaceDocument[]>} all persisted storage spaces
     */
    @Get('/:id', passport.authenticate('jwt', {session: false}))
    public async getStorageSpace(request: Request): Promise<any> {
        const storageSpaceId = validateMongoId(request.params.id);
        const userId = request.user.userId;
        const item = await this.storageSpaceService.getStorageSpace(userId, storageSpaceId);
        return Serialize(item, StorageSpaceDocument);
    }

    /**
     * Creates a new storage space.
     * @param   request storage space
     * @returns {Promise<NewStorageSpaceInput>} persisted storage space
     */
    @Post('/', passport.authenticate('jwt', {session: false}))
    public async createStorageSpace(request: Request, response: Response): Promise<any> {
        // Validate and deserialize request, enrich with userId from JWT token
        let storageSpaceRequest = await validateBody(request, StorageSpaceRequest);
        storageSpaceRequest.userId = request.user.userId;

        // save storage space
        const item = await this.storageSpaceService.createStorageSpace(storageSpaceRequest);

        // return storage space to the user
        response.status(201);
        return Serialize(item, StorageSpaceDocument);
    }

    /**
     * Updates the persisted storage space with the given id.
     * @param   request id and body
     * @returns {Promise<void>}
     */
    @Put('/:id', passport.authenticate('jwt', {session: false}))
    public async updateStorageSpace(request: Request, response: Response): Promise<void> {
        const id = validateMongoId(request.params.id);
        const storageSpaceRequest = await validateBody(request, StorageSpaceRequest);
        await this.storageSpaceService.updateStorageSpace(id, storageSpaceRequest);
        response.sendStatus(200);
    }

    /**
     * Deletes the persisted storage space with the given id.
     * @param   request id
     * @returns {Promise<void>}
     */
    @Delete('/:id', passport.authenticate('jwt', {session: false}))
    public async deleteStorageSpace(request: Request, response: Response): Promise<void> {
        const id = validateMongoId(request.params.id);
        await this.storageSpaceService.deleteStorageSpace(id);
        response.sendStatus(200);
    }
}
