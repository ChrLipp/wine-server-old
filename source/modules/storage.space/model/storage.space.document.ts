import {serialize, serializeAs} from 'cerialize';
import {Instance, Collection, Index, Property, ObjectID} from 'iridium';

/**
 * Interface of the storage space.
 */
export interface IStorageSpaceDocument  {
    /** ID of the document */
    _id?: string;

    /** ID of the user */
    userId: string;

    /**
     * The name of the storage space. The user is responsible to setup a possible hierarchie
     * via the name, e.g.
     * - row 1, column 1
     * - ...
     * - row 4, column 4
     */
    name: string;

    /** Timestamp of the last inventory. */
    lastInventory?: Date;

    /** Current amount of bottles in the storage space. */
    currentAmount?: number;

    /** Maximal amount of bottles which have ever been in the storage space. */
    maximalAmount?: number;
}

/**
 * Storage space document.
 */
@Index({ userId: 1, name: 1 }, { unique: true })
@Collection('StorageSpace')
export class StorageSpaceDocument
    extends Instance<IStorageSpaceDocument, StorageSpaceDocument>
    implements IStorageSpaceDocument {

    /** MongoDB document id */
    @serializeAs(String, 'id')
    @ObjectID
    public _id: string;

    /** ID of the user */
    @ObjectID
    public userId: string;

    /** Name of the storage space */
    @serialize
    @Property(String)
    public name: string;

    /**
     * The date the last inventory was performed.
     * When the storage space is created, the last inventory date is the creation date.
     */
    @serialize
    @Property(Date)
    public lastInventory?: Date;

    /**
     * The current amount of bottles in the storage space.
     * When the storage space is created, the current amount is set to zero.
     */
    @serialize
    @Property(Number)
    public currentAmount: number;

    /**
     * The maximal amount of bottles in the storage space.
     * When the storage space is created, the maximal amount is set to zero.
     */
    @serialize
    @Property(Number)
    public maximalAmount: number;

    /**
     * Creation callback for initialising the document.
     * @param doc document to initialise
     */
    public static onCreating(doc: StorageSpaceDocument) {
        doc.currentAmount = 0;
        doc.maximalAmount = 0;
        doc.lastInventory = new Date();
    }
}
