import * as request from 'supertest';

import {Application} from '../../../application';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';

describe('Healthcheck api', () => {
    let app: Application;

    before((done) => {
        app = <Application> container.get(TYPES.Application);
        app.init().then(() => {
            done();
        });
    });

    it('returns OK', (done) => {
        request(app.getExpressServer())
            .get('/api/health')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

    after((done) => {
        app.prepareShutdown().then(() => {
            done();
        });
    });
});
