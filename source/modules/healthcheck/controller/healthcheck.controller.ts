import {interfaces, Controller, Get} from 'inversify-express-utils';
import {injectable} from 'inversify';

import {LoggerFactory} from '../../../core/logging/logger.factory';

/**
 * Health check controller.
 */
@Controller('/api/health')
@injectable()
export class HealthCheckController implements interfaces.Controller {
    /** logger instance */
    private readonly log = LoggerFactory.getLogger(__filename);

    /**
     * @apiGroup misc
     * @apiName health
     * @api {get} /api/health Health check
     * @apiSuccess {String} result Is always 'OK'.
     */
    /**
     * Health check method which just returns OK.
     * @returns {{result: string}}
     */
    @Get('/')
    public index(): { result: string } {
        this.log.info('Inside api/health');
        return { result: 'OK' };
    }
}
