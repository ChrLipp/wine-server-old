import {deserialize} from 'cerialize';
import {IsEmail, MinLength, IsNotEmpty, Validate} from 'class-validator';

import {IsEqualTo} from '../../../core/error/validation/is.equal.validation';
import {TemporaryEmailValidator} from '../../../core/error/validation/temporary.email.validator';

/**
 * User registration request.
 */
export class UserRegistrationRequest {
    /** Name of the user */
    @deserialize
    @MinLength(3)
    public name: string;

    /** EMail of the user */
    @deserialize
    @IsEmail()
    @Validate(TemporaryEmailValidator)
    public email: string;

    /** Cleartext password of the user, 1st entry */
    @deserialize
    @IsNotEmpty()
    @IsEqualTo('password2', {message: 'Passwords must be equal'})
    public password: string;

    /** Cleartext password of the user, 2nd entry */
    @deserialize
    @IsNotEmpty()
    public password2: string;
}
