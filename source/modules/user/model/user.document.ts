import {Instance, Collection, Index, ObjectID, Property, Transform} from 'iridium';

import {config} from '../../../core/config/config';

/**
 * State of an EMail for a user who registered with username/password.
 */
export enum EmailState {
    /** Mail is unconfirmed (immediately after creation */
    unconfirmed,

    /** Mail is confirmed */
    confirmed
};

/**
 * Interface of a last login update.
 */
export interface ILastLogin {
    /** TS of last login of the user */
    lastLogin?: Date;

    /** Alternative access for facebookUser/twitterUser */
    [key: string]: any;
};

/**
 * Interface of the user.
 */
export interface IUserDocument  extends ILastLogin {

    /** ID of the user */
    _id?: string;

    /** Name of the user */
    name: string;

    /** EMail of the user */
    email: string;

    /** Specific data for user with Username/Password registration strategy */
    localUser?: {
        /** TS email was sended */
        emailSentOn?: Date;

        /** State of the mail */
        emailState: EmailState;

        /** Hashed password */
        hashedPassword: string;

        /** Random Hash for email notification */
        randomHash?: string;

        /** Salt */
        salt: string;
    };

    /** Specific data for user with Facebook registration strategy */
    facebookUser?: {
        /** Facebook internal id identificating the user */
        id: string;

        /** Facebook token */
        token: string;
    };

    /** Specific data for user with Twitter registration strategy */
    twitterUser?: {
        /** Twitter internal id identificating the user */
        id: string;

        /** Twitter token */
        token: string;
    };
}

/**
 * User document.
 */
@Index({ name: 1 }, { unique: true })
@Index({ email: 1 }, { unique: true })
@Index({ 'localUser.randomHash': 1 }, { sparse: true, unique: true })
@Index({ 'facebookUser.id': 1 }, { sparse: true, unique: true })
@Index({ 'twitterUser.id': 1 }, { sparse: true, unique: true })
@Index({ 'localUser.emailSentOn': 1}, {
    expireAfterSeconds: config.mail.registration.confirmValidity * 60,
})
@Collection('User')
export class UserDocument
extends Instance<IUserDocument, UserDocument>
implements IUserDocument {

    /** MongoDB document id */
    @ObjectID
    public _id: string;

    /** Name of the user */
    @Property(String)
    public name: string;

    /** EMail of the user */
    @Property(String)
    @Transform(s => s, s => s.toLowerCase())
    public email: string;

    /** TS of last login of the user */
    @Property(Date, false)
    public lastLogin?: Date;

    /** Specific data for user with Username/Password registration strategy */
    @Property({
        emailSentOn:    Date,
        emailState:     EmailState,
        hashedPassword: String,
        randomHash:     String,
        salt:           String
    }, false)
    public localUser?: {
        /** TS email was sended */
        emailSentOn?: Date,

        /** State of the mail */
        emailState: EmailState;

        /** Hashed password of the user */
        hashedPassword: string;

        /** Random Hash for email notification */
        randomHash?: string;

        /** Salt */
        salt: string;
    };

    /** Specific data for user with Facebook registration strategy */
    @Property({
        id:             String,
        token:          String
    }, false)
    public facebookUser?: {
        /** Facebook internal id identificating the user */
        id: string;

        /** Facebook token */
        token: string;
    };

    /** Specific data for user with Twitter registration strategy */
    @Property({
        id:             String,
        token:          String
    }, false)
    public twitterUser?: {
        /** Twitter internal id identificating the user */
        id: string;

        /** Twitter token */
        token: string;
    };

    /**
     * Creation callback for initialising the document.
     * @param doc document to initialise
     */
    public static onCreating(doc: UserDocument) {
        if (doc.localUser) {
            doc.localUser.emailSentOn = new Date();
        }
    }
}
