import * as passport from 'passport';

import {injectable, inject} from 'inversify';
import {Controller, Post, Get} from 'inversify-express-utils';
import {Request, Response} from 'express';
import {MongoError} from 'mongodb';
import {Validator} from 'class-validator';

import {config} from '../../../core/config/config';
import {generateJwt} from '../../../core/auth/jwt';
import {TYPES} from '../../../ioc/types';
import {UserService} from '../service/user.service';
import {UserRegistrationRequest} from '../request/user.registration.request';
import {validateBody} from '../../../core/util/controller.validate';
import {ValidationException} from '../../../core/error/exception/validation.exception';

/**
 * User controller.
 */
@Controller('/api/user')
@injectable()
export class UserController {
    /**
     * Ctor.
     * @param userService The user service
     */
    constructor(
        @inject(TYPES.UserService) private userService: UserService) {
    }

    /**
     * @apiGroup user
     * @apiName register
     * @api {post} /api/user/register Register a new user
     * @apiParam {Object} nested UserRegistrationRequest
     * @apiParam {String} nested.name Name of the user
     * @apiParam {String} nested.email EMail of the user
     * @apiParam {String} nested.password Cleartext password of the user, 1st entry
     * @apiParam {String} nested.password2 Cleartext password of the user, 2st entry
     * @apiSuccess (201) none
     */
    /**
     * Register a new user.
     * @param   request for a new user
     * @returns {Promise<void>}
     */
    @Post('/register')
    public async register(request: Request, response: Response): Promise<void> {
        try {
            // deserialize and validate the request
            const userRegistration: UserRegistrationRequest =
                await validateBody(request, UserRegistrationRequest);

            // perform the registration
            await this.userService.register(userRegistration);
            response.sendStatus(201);
        }
        catch (error) {
            if (error instanceof MongoError) {
                throw new ValidationException(
                    'A user with the same mail and/or user id already exists.');
            }
            throw error;
        }
    }

    /**
     * @apiGroup user
     * @apiName confirm
     * @api {get} /api/user/confirm/:id Confirm an user
     * @apiParam {String} id Users unique ID.
     * @apiSuccess (201) none
     */
    /**
     * Confirms a registration entry.
     * @param request   Registration info
     * @param response  status code 200
     * @returns {Promise<void>}
     */
    @Get('/confirm/:id')
    public async confirm(request: Request, response: Response): Promise<void> {
        const id = request.params.id;
        const validator = new Validator();
        if (!(validator.isAlphanumeric(id) && validator.length(id, 40, 40))) {
            throw new ValidationException('Invalid confirmation id.');
        }

        await this.userService.confirm(id);
        response.sendStatus(200);
    }

    /**
     * @apiGroup user
     * @apiName login
     * @api {post} /api/user/login Performs a login
     * @apiParam {Object} nested passport login request
     * @apiParam {String} nested.username Username
     * @apiParam {String} nested.password Password
     * @apiSuccess (200) none
     */
    /**
     * Performs a login.
     * @param request Username/Password
     * @param response JWT header, status code 200
     * @returns {Promise<void>}
     */
    @Post('/login', passport.authenticate('local', {session: false}))
    public async login(request: Request, response: Response) {
        response.header('JWT', generateJwt(request.user));
        response.sendStatus(200);
    }

    /**
     * @apiGroup user
     * @apiName facebook
     * @api {post} /api/user/facebook Login with Facebook
     * @apiSuccess (302) none Redirects to Facebook for authentification
     */
    /**
     * Login/register the app with Facebook.
     * Passport performs a redirect to the Facebook login page.
     * @returns {Promise<void>}
     */
    @Get('/facebook', passport.authorize('facebook', {
        scope : ['email'],
        state:  true
    }))
    public async facebook(): Promise<void> {
    }

    /**
     * @apiGroup user
     * @apiName fbcallback
     * @api {post} /api/user/fbcallback Facebook callback
     * @apiParam {Object} unknown Facebook specific params
     * @apiSuccess (302) none Redirects to sucess/error page
     * @apiSampleRequest off
     */
    /**
     * Generates a JWT token for a Facebook authenticated user and performs a redirect.
     * @param request   Facebook callback params
     * @param response  Redirect to sucess/error page
     * @returns {Promise<void>}
     */
    @Get('/fbcallback', passport.authenticate('facebook', {
        failureRedirect: `/${config.auth.oauth.errorUrl}`,
        session: false
    }))
    public async facebookCallback(request: Request, response: Response): Promise<void> {
        const jwt = generateJwt(request.user);
        response.redirect(`/${config.auth.oauth.sucessUrl}?token=${jwt}`);
    }

    /**
     * @apiGroup user
     * @apiName twitter
     * @api {post} /api/user/twitter Login with Twitter
     * @apiSuccess (302) none Redirects to Twitter for authentification
     */
    /**
     * Login/register the app with Twitter.
     * Since Twitter is using OAuth1, this is the only route with activated session.
     * Passport performs a redirect to the twitter login page.
     * @returns {Promise<void>}
     */
    @Get('/twitter', passport.authorize('twitter'))
    public async twitter(): Promise<void> {
    }

    /**
     * @apiGroup user
     * @apiName twittercallback
     * @api {post} /api/user/twittercallback Twitter callback
     * @apiParam {Object} unknown Twitter specific params
     * @apiSuccess (302) none Redirects to sucess/error page
     * @apiSampleRequest off
     */
    /**
     * Generates a JWT token for a Twitter authenticated user and perfor ms a redirect.
     * @param request   Twitter callback params
     * @param response  Redirect to Sucess/Error page
     * @returns {Promise<void>}
     */
    @Get('/twittercallback', passport.authenticate('twitter', {
        failureRedirect: `/${config.auth.oauth.errorUrl}`,
        session: false
    }))
    public async twitterCallback(request: Request, response: Response): Promise<void> {
        const jwt = generateJwt(request.user);
        response.redirect(`/${config.auth.oauth.sucessUrl}?token=${jwt}`);
    }

    /**
     * @apiGroup user
     * @apiName google
     * @api {post} /api/user/google Login with Google
     * @apiSuccess (302) none Redirects to Google for authentification
     */
    /**
     * Login/register the app with Google.
     * Passport performs a redirect to the Google login page.
     * @returns {Promise<void>}
     */
    @Get('/google', passport.authorize('google'))
    public async google(): Promise<void> {
    }

    /**
     * @apiGroup user
     * @apiName googlecallback
     * @api {post} /api/user/googlecallback Google callback
     * @apiParam {Object} unknown Google specific params
     * @apiSuccess (302) Reirects to sucess/error page
     * @apiSampleRequest off
     */
    /**
     * Generates a JWT token for a Google authenticated user and performs a redirect.
     * @param request   Google callback params
     * @param response  Redirect to Sucess/Error page
     * @returns {Promise<void>}
     */
    @Get('/googlecallback', passport.authenticate('google', {
        failureRedirect: `/${config.auth.oauth.errorUrl}`,
        session: false
    }))
    public async googleCallback(request: Request, response: Response): Promise<void> {
        const jwt = generateJwt(request.user);
        response.redirect(`/${config.auth.oauth.sucessUrl}?token=${jwt}`);
    }
}
