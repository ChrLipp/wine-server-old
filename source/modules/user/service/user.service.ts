import {inject, injectable} from 'inversify';
import * as bluebird from 'bluebird';
import * as crypto from 'crypto';
import * as path from 'path';

import {config} from '../../../core/config/config';
import {hashPassword} from '../../../core/auth/hash';
import {IUserDocument, EmailState} from '../model/user.document';
import {MailService} from '../../../core/mail/mail.service';
import {NotFoundException} from '../../../core/error/exception/notfound.exception';
import {TYPES} from '../../../ioc/types';
import {UserRegistrationRequest} from '../request/user.registration.request';
import {UserRepository} from '../repository/user.repository';

const randomBytes = bluebird.promisify(crypto.randomBytes);

/**
 * User service.
 */
@injectable()
export class UserService {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(
        @inject(TYPES.UserRepository)   private userRepository: UserRepository,
        @inject(TYPES.MailService)      private mailService: MailService) {
    }

    /**
     * Performs the registration.
     * @param userRequest registration request
     * @returns {Promise<IUserDocument>} the created user
     */
    public async register(userRequest: UserRegistrationRequest): Promise<void> {
        // create
        let userDocument = await this.mapRequestToDocument(userRequest);
        let result = await this.userRepository.create(userDocument);

        // send email
        await this.mailService.send({
            from:               'DoNotReply <sender@server.com>',
            params:         {
                companyName:    'Mein Weinkeller',
                confirmLink:    config.server.url +
                                config.mail.registration.confirmLink +
                                result.localUser.randomHash
            },
            subject:            'Registration',
            templatePath:       path.normalize(__dirname + '/../view/registration.hbs'),
            to:                 result.email
        });
    }

    /**
     * Confirms a registration email.
     * If the registration is not confirmed within config.mail.registration.confirmValidity
     * minutes, the registration expires (and is removed automatically by MongoDB).
     * Therefor only unconfirmed entries with non expired hashes can be found.
     * @param randomHash        Short hash for confirmation link
     * @returns {Promise<void>}
     */
    public async confirm(randomHash: string): Promise<void> {
        // load entry by hash
        const entry = await this.userRepository.findOne({
            'localUser.randomHash': {$eq: randomHash}
        });
        if (!entry) {
            throw new NotFoundException('Hash does not exist.');
        }

        // update emailState and remove the attributes emailSentOn and randomHash
        // by not providing them
        const update = {
            localUser: {
                emailState:     EmailState.confirmed,
                hashedPassword: entry.localUser.hashedPassword,
                salt:           entry.localUser.salt
            }
        };
        return this.userRepository.update(entry._id, update);
    }

    /**
     * Maps the registration request to the user document.
     * Poor man's mapper, manual mapped.
     * @param userRequest registration request
     * @returns {IUserDocument}
     */
    private async mapRequestToDocument(request: UserRegistrationRequest): Promise<IUserDocument > {
        // hash password
        const [randomHash, salt] = await Promise.all([randomBytes(20), randomBytes(128)]);
        const password = await hashPassword(request.password, salt);

        // create document
        let userDocument: IUserDocument = {
            email:              request.email,
            localUser: {
                emailState:     EmailState.unconfirmed,
                hashedPassword: password,
                randomHash:     randomHash.toString('hex'),
                salt:           salt.toString('hex')
            },
            name:               request.name
        };
        return userDocument;
    }
}
