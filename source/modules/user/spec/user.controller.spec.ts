import 'reflect-metadata';
import * as chai from 'chai';
import * as sinon from 'sinon';
import * as sinonChai from 'sinon-chai';
import {expect} from 'chai';
import {Request, Response} from 'express';

/* tslint:disable:no-var-requires */
const mockRes = require('sinon-express-mock').mockRes;
/* tslint:enable:no-var-requires */

import {config} from '../../../core/config/config';
import {UserController} from '../controller/user.controller';
import {UserService} from '../service/user.service';
import {ValidationException} from '../../../core/error/exception/validation.exception';

chai.use(sinonChai);

describe('User controller', () => {
    let controller: UserController;
    let serviceStub: any;

    before(() => {
        serviceStub  = <any> sinon.createStubInstance(UserService);
        controller = new UserController(<UserService> <any> serviceStub);
    });

    describe('register', () => {
        it('A to short name should result in a validation error', async () => {
            const request = {
                body: {
                    email:      'a@b.com',
                    name:       '1',
                    password:   '123',
                    password2:  '123'
                }
            };
            return controller
                .register(<Request> request, <Response> {})
                .catch((error) => {
                    expect(error instanceof ValidationException).to.be.true;
                    expect(serviceStub.register).to.have.not.been.called;
                });
        });

        it('Not equal passwords should result in a validation error', async () => {
            const request = {
                body: {
                    email:      'a@b.com',
                    name:       '123',
                    password:   '123',
                    password2:  '321'
                }
            };
            return controller
                .register(<Request> request, <Response> {})
                .catch((error) => {
                    expect(error instanceof ValidationException).to.be.true;
                    expect(serviceStub.register).to.have.not.been.called;
                });
        });

        it('Invalid mail adresses should result in a validation error', async () => {
            const request = {
                body: {
                    email:      'a@b',
                    name:       '123',
                    password:   '123',
                    password2:  '123'
                }
            };
            return controller
                .register(<Request> request, <Response> {})
                .catch((error) => {
                    expect(error instanceof ValidationException).to.be.true;
                    expect(serviceStub.register).to.have.not.been.called;
                });
        });

        it('Mailinator mail adresses should result in a validation error', async () => {
            // store original config setting
            const allowTempMails = config.mail.allowTempMails;
            config.mail.allowTempMails = false;

            // perform test
            const request = {
                body: {
                    email:      'a@mailinator.com',
                    name:       '123',
                    password:   '123',
                    password2:  '123'
                }
            };

            return controller
                .register(<Request> request, <Response> {})
                .catch((error) => {
                    expect(error instanceof ValidationException).to.be.true;
                    expect(serviceStub.register).to.have.not.been.called;
                })
                .then(() => {
                    // restore original config setting
                    config.mail.allowTempMails = allowTempMails;
                });
        });

        it('A correct response should call the service layer', async () => {
            const request = {
                body: {
                    email:      'a@b.com',
                    name:       '123',
                    password:   '123',
                    password2:  '123'
                }
            };
            return controller
                .register(<Request> request, mockRes())
                .then(() => {
                    expect(serviceStub.register).to.have.been.called;
                });
        });
    });

    describe('confirm', () => {
        it('An id with length other than 40 should result in a validation error', async () => {
            const request = {
                params: {
                    id: '1'
                }
            };

            return controller
                .confirm(<Request> request, <Response> {})
                .catch((error) => {
                    expect(error instanceof ValidationException).to.be.true;
                    expect(serviceStub.confirm).to.have.not.been.called;
                });
        });
    });
});
