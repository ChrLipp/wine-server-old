/* tslint:disable:no-var-requires */
const request = require('supertest-as-promised');
/* tslint:enable:no-var-requires */
import {expect} from 'chai';

import {Application} from '../../../application';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';
import {UserHelper} from './user.helper';
import {IUserDocument, EmailState} from '../model/user.document';

describe('User api - register', function() {
    this.timeout(4000);

    let app: Application;
    const userHelper = new UserHelper();

    before(async () => {
        app = <Application> container.get(TYPES.Application);
        await app.init();
    });

    beforeEach(async () => {
        await userHelper.removeUser();
    });

    it('creates an user', async () => {
        const userRequest = {
            email :     userHelper.testUser,
            name:       'marcus',
            password:   '123',
            password2:  '123'
        };
        await request(app.getExpressServer())
            .post('/api/user/register')
            .send(userRequest)
            .set('Accept', 'application/json')
            .expect(201);

        const userDoc = await userHelper.getUser();
        if (userDoc && userDoc.localUser) {
            expect(userDoc.email).to.be.equal(userHelper.testUser.toLowerCase());
            expect(userDoc.localUser.hashedPassword).not.to.be.equal(userRequest.password);
            expect(userDoc.localUser.hashedPassword.length).to.be.equal(1024);
            expect(userDoc.localUser.emailState).to.be.equal(EmailState.unconfirmed);
            expect(userDoc.localUser.randomHash).to.not.be.null;
            expect(userDoc.localUser.emailSentOn).to.not.be.null;
            expect(userDoc.localUser.salt).to.not.be.null;
        }
        return true;
    });

    it('fails if the user is already existing', async () => {
        const userDocument: IUserDocument = {
            email:              userHelper.testUser.toLowerCase(),
            localUser: {
                emailState:     EmailState.unconfirmed,
                hashedPassword: 'ABC',
                randomHash:     'ABC',
                salt:           'ABC'
            },
            name:               'Given'
        };
        await userHelper.saveUser(userDocument);

        const userRequest = {
            email :     userHelper.testUser,
            name:       'marcus',
            password:   '123',
            password2:  '123'
        };
        await request(app.getExpressServer())
            .post('/api/user/register')
            .send(userRequest)
            .set('Accept', 'application/json')
            .expect(400);
    });

    afterEach(async () => {
        return userHelper.removeUser();
    });

    after(async () => {
        return app.prepareShutdown();
    });
});
