import {expect} from 'chai';
import * as jwt from 'jsonwebtoken';

import {Application} from '../../../application';
import {config} from '../../../core/config/config';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';
import {UserDocument} from '../model/user.document';
import {UserHelper} from './user.helper';

describe('User api - login', function() {
    let app: Application;
    let user: UserDocument;

    const userHelper = new UserHelper();

    before(async () => {
        app = <Application> container.get(TYPES.Application);
        await app.init();
    });

    beforeEach(async () => {
        await userHelper.removeUser();
        user = await userHelper.createUser();
    });

    it('Performs a login', async () => {
        // Precondition
        expect(user.lastLogin).to.be.undefined;

        // Login
        const response = await userHelper.login(app);
        expect(response.header).to.have.property('jwt');

        // Postconditions
        const newUser = await userHelper.getUser();
        expect(newUser).to.not.be.null;
        if (newUser) {
            expect(newUser.lastLogin).to.be.a('date');
        }

        // JWT content
        const decodedJwt = jwt.verify(response.header.jwt, config.auth.local.secret);
        expect(decodedJwt.userId).to.be.equal(user._id);
        expect(decodedJwt.email).to.be.equal(user.email);
        expect(decodedJwt.name).to.be.equal(user.name);

        return true;
    });

    afterEach(async () => {
        return userHelper.removeUser();
    });

    after(async () => {
        return app.prepareShutdown();
    });
});
