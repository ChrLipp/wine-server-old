/* tslint:disable:no-var-requires */
const request = require('supertest-as-promised');
/* tslint:enable:no-var-requires */
import {expect} from 'chai';

import {Application} from '../../../application';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';
import {IUserDocument, EmailState} from '../model/user.document';
import {UserHelper} from './user.helper';

describe('User api - confirm', function() {
    let app: Application;
    const userHelper = new UserHelper();

    before(async function() {
        app = <Application> container.get(TYPES.Application);
        await app.init();
    });

    beforeEach(async function() {
        await userHelper.removeUser();
    });

    it('confirms an user', async () => {
        const randomHash = '1234567890123456789012345678901234567890';
        const userDocument: IUserDocument = {
            email:              userHelper.testUser.toLowerCase(),
            localUser: {
                emailState:     EmailState.unconfirmed,
                hashedPassword: 'ABC',
                randomHash:     randomHash,
                salt:           'ABC'
            },
            name:               'Given'
        };
        await userHelper.saveUser(userDocument);

        await request(app.getExpressServer())
            .get(`/api/user/confirm/${randomHash}`)
            .set('Accept', 'application/json')
            .expect(200);

        const userDoc = await userHelper.getUser();
        if (userDoc && userDoc.localUser) {
            expect(userDoc.localUser.emailState).to.be.equal(EmailState.confirmed);
            expect(userDoc.localUser.randomHash).to.be.undefined;
            expect(userDoc.localUser.emailSentOn).to.be.undefined;
        }

        await request(app.getExpressServer())
            .get(`/api/user/confirm/${randomHash}`)
            .set('Accept', 'application/json')
            .expect(404);
    });

    afterEach(async function() {
        return userHelper.removeUser();
    });

    after(async function() {
        return app.prepareShutdown();
    });
});
