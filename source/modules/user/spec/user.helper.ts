/* tslint:disable:no-var-requires */
import supertest = require('supertest');
const request = require('supertest-as-promised');
/* tslint:enable:no-var-requires */
import Test = supertest.Test;

import {Application} from '../../../application';
import {Database} from '../../../core/database/database';
import {IUserDocument, UserDocument, EmailState} from '../model/user.document';
import container from '../../../ioc/bootstrap';
import {TYPES} from '../../../ioc/types';

/**
 * Helper methods for testing the user.
 */
export class UserHelper {
    /** Database for direct MongoDB manipulation */
    private database: Database;

    /** Test user data for tests. This email MUST NOT be used in production. */
    public readonly testUser: string = 'test@CORBA.AT';

    /**
     * Ctor.
     */
    public constructor() {
        this.database = <Database> container.get(TYPES.Database);
    }

    /**
     * Returns the user from MongoDB.
     * @returns {Promise<Bluebird<null|UserDocument>>}
     */
    public async getUser(): Promise<UserDocument | null> {
        return this.database.users.findOne({'email': this.testUser.toLowerCase()});
    }

    /**
     * Deletes the user from MongoDB.
     * @returns {Promise<void>}
     */
    public async removeUser(): Promise<void> {
        let userDoc = await this.getUser();
        if (userDoc) {
            await this.database.users.remove(userDoc._id);
        }
    }

    /**
     * Saves a user document.
     */
    public async saveUser(userDocument: IUserDocument): Promise<UserDocument> {
        return this.database.users.insert(userDocument);
    }

    /**
     * Creates a user. The data is hardcoded, the email is confirmed.
     * @returns {Promise<UserDocument>}
     */
    public async createUser(): Promise<UserDocument> {
        /* tslint:disable:max-line-length */
        const userDocument: IUserDocument = {
            email: this.testUser.toLowerCase(),
            localUser: {
                emailState: EmailState.confirmed,
                hashedPassword: '5672389dd4242a0d9ff1cd39c4cf7dafdaddbe745b914c5d3bad044ffdd7c774f3f66c9a6f636274669c0e02b62502698e5566f1ca1807799e12b6311681270f42f3bca1ace9a1816d316aca9ebc593a6afe8c48fd70ae11111a1c9aee2f13ba1bc13a3422c87e9463ac4aaa7692d430782b3d5aeef29ccb0ec2ddbd65f827e0ad7c9f6e398fe0b055f6121726a47e9df28d2bf6c39422dcd7ded5f19eae2f1b3db13397a58e4920e0aa113ed47ffd25ac582b76a152b01820973d588c4f13ef4fbc5ae7180d09a7761b31fce8632d4e9d3577968864ad5904e05481857ecf39c23e40aef43a3a8e216a5f4d6da5384a2b8ee7ef21ae48957f6b834d5d56e13b601c5fcc2d5ec19c8facc58519b5fe342dce860835cd0ecae5e43dbe723fcdc92d0e33b02f959f3cd190c5b73e263a6f7848dc37740c0902376ab577c7832b449c1261973637866886cfb6ebbb4bcdabacbd18bea03238a97943752337caf638d016db96ead7d8f19273f4db4dc17e5be26ba5acb8bd8041a1bf9e4a6bc9820c1a06f0b77b3df8d92a6097e0463b2360947e25ba24714116850a3dbc1818b543850fdc4d348b3f9f061e8188260ad955a0751fa2b32f5e21a512e257d3cee3a90d2a9a7c47b8a0a134bf76c218789ed9bf5137d0e06fd8e31f8772a1029a3cd0f2afe7befec98e5dd278e006e6c12dd41b803e944ddbc10096f4bf7641c264c9',
                randomHash: 'must be provided to store an user',
                salt: 'b1a6f5aaab23fd3f19610445b457706cae9bde2d476366143bc580958bb5233378f53cbcfcacf5ddcf3ea17475362dd111124f3acd71a2bcc4115a25ba980a6fabdfd3066a0fa2aff26f6dbb6411f0de8be1b929066301d194140c45754384fceb94193c2f8464e4fb96b46323942e82a9d0aa1881a4b7076e834bf00bf50d75'
            },
            name: 'Given'
        };
        /* tslint:enable:max-line-length */
        return this.saveUser(userDocument);
    }

    /**
     * Performs a login for the test user.
     * @param app
     * @returns {Promise<this|Assertion>}
     */
    public async login(app: Application): Promise<Test> {
        return await request(app.getExpressServer())
            .post(`/api/user/login`)
            .send({
                password: '123',
                username: this.testUser
            })
            .expect(200);
    }
}
