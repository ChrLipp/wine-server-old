import {injectable, inject} from 'inversify';

import {BaseRepository} from '../../../core/base/base.repository';
import {Database} from '../../../core/database/database';
import {TYPES} from '../../../ioc/types';
import {IUserDocument, UserDocument} from '../model/user.document';

@injectable()
export class UserRepository
extends BaseRepository<IUserDocument, UserDocument> {
    /**
     * Ctor.
     * @param database Database singleton
     */
    constructor(@inject(TYPES.Database) database: Database) {
        super();
        this.model = database.users;
    }
}
