import {inject, injectable} from 'inversify';
import {Core} from 'iridium';
import {InversifyExpressServer} from 'inversify-express-utils';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as express from 'express';
import * as session from 'express-session';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as passport from 'passport';
import * as path from 'path';

/* tslint:disable:no-var-requires */
const expressWinston = require('express-winston');
/* tslint:enable:no-var-requires */

import {AuthentificationMiddleware} from './core/auth/authentification.middleware';
import {baseExceptionErrorHandler} from './core/error/handler/base.exception.handler';
import {config} from './core/config/config';
import {Database} from './core/database/database';
import {errorHandler} from './core/error/handler/error.handler';
import container from './ioc/bootstrap';
import {LoggerFactory} from './core/logging/logger.factory';
import {TYPES} from './ioc/types';

/**
 * Server application.
 */
@injectable()
export class Application {
    /** logger instance */
    private readonly log = LoggerFactory.getLogger(__filename);

    /** Express server */
    private expressServer: express.Application;

    /**
     * Ctor.
     * @param database
     * @param auth
     */
    constructor(
        @inject(TYPES.Database)                     private database: Database,
        @inject(TYPES.AuthentificationMiddleware)   private auth: AuthentificationMiddleware) {
    }

    /**
     * Connects to MongoDB and sets up (not starting) the server.
     * @returns {express.Application}
     */
    public async init(): Promise<express.Application> {
        try {
            await this.connectToMongo();
            return this.setupServer();
        }
        catch (ex) {
            console.error('Error while starting the server! ' + ex);
            throw ex;
        }
    }

    /**
     * Starts the server.
     */
    public run(): void {
        let port = config.server.port;
        this.expressServer
            .listen(port, () => {
                this.log.info(
                    `Running under node version ${process.version} (${config.environment.name})`);
                this.log.info('Express server listening on port %d', port);
            });
    }

    /**
     * Returns the internal express application.
     * @returns {express.Application}
     */
    public getExpressServer(): express.Application {
        return this.expressServer;
    }

    /**
     * Frees all ressources to allow express to shutdown.
     * @returns {Promise<Core>} Iridium MongoDB core
     */
    public async prepareShutdown(): Promise<Core> {
        return Promise.resolve(this.database.close());
    }

    /**
     * Connect to MongoDB.
     * @returns {Promise<Core>} Iridium MongoDB core
     */
    private async connectToMongo(): Promise<Core> {
        return Promise.resolve(this.database.connect());
    }

    /**
     * Sets up the express server.
     * @returns {Promise<InversifyExpressServer>} The server
     */
    private setupServer(): express.Application {
        let server = new InversifyExpressServer(container);
        this.expressServer = server
            .setConfig((app: express.Application) => {
                this.setupStandardMiddleware(app);
            })
            .setErrorConfig((app: express.Application) => {
                this.setupErrorMiddleware(app);
            })
            .build();

        return this.expressServer;
    }

    /**
     * Config server middleware.
     * @param app Express application
     */
    private setupStandardMiddleware(app: express.Application): void {
        // Heroku doesn't compress, so compression must be part of the application
        app.use(compression());

        // enrich request body with parsed fields
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));

        // Helmet for security headers and checks
        app.use(helmet());

        // add request/response logging
        app.use(morgan('short', {
            stream: LoggerFactory.getMorganStreamAdapter(this.log)
        }));
        app.use(expressWinston.logger({
            level: 'verbose',
            winstonInstance: LoggerFactory.getInternalLogger()
        }));

        // passport for authentification / authorisation
        app.use(this.auth.init());

        // session support for OAuth only
        app.use(session({
            cookie:             { secure: config.environment.isProd },
            proxy:              true,
            resave:             false,
            saveUninitialized:  false,
            secret:             config.auth.oauth.sessionSecret,
        }));
        app.use(passport.session());
        app.enable('trust proxy');

        // and finally static file serving
        const staticDir = path.normalize(__dirname + '/../../public');
        app.use(express.static(staticDir));
        this.log.info(`Serving static files from directory '${staticDir}'`);
    }

    /**
     * Config server error middleware.
     * @param app Express application
     */
    private setupErrorMiddleware(app: express.Application): void {
        app.use(baseExceptionErrorHandler);
        app.use(errorHandler);
    }
}


