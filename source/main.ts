// create logger as first line in app to set the root dir
import {LoggerFactory} from './core/logging/logger.factory';
LoggerFactory.getLogger(__filename);

import {Application} from './application';
import container from './ioc/bootstrap';
import {TYPES} from './ioc/types';

/**
 * Starts the server.
 */
async function main(): Promise<void> {
    const app = <Application> container.get(TYPES.Application);
    try {
        await app.init();
    }
    catch (ex) {
        return;
    }
    app.run();
}

main();
