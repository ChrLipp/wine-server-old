import {Instance, Collection, Property, ObjectID} from 'iridium';

/**
 * Interface of the Log document.
 */
export interface ILogDocument  {
    /** ID of the document */
    _id?: string;

    /** Message text */
    message: string;

    /** Level of the log entry */
    level: string;

    /** Timestamp the log was performed */
    timestamp: Date;

    /** Optional meta data */
    metadata: any;
}

/**
 * Log document.
 */
@Collection('Log')
export class LogDocument
extends Instance<ILogDocument, LogDocument>
implements ILogDocument {

    /** MongoDB document id */
    @ObjectID
    public _id: string;

    /** Message text */
    @Property(String)
    public message: string;

    /** Level of the log entry */
    @Property(String)
    public level: string;

    /** Timestamp the log was performed */
    @Property(Date)
    public timestamp: Date;

    /** Optional meta data */
    @Property(false)
    public metadata: any;
}
