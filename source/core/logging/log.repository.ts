import {injectable, inject} from 'inversify';

import {BaseRepository} from '../../core/base/base.repository';
import {Database} from '../../core/database/database';
import {ILogDocument, LogDocument} from './log.document';
import {TYPES} from '../../ioc/types';

/**
 * Log repository.
 * The repository has to initialise the model.
 */
@injectable()
export class LogRepository extends BaseRepository<ILogDocument, LogDocument> {
    /**
     * Ctor.
     * @param database Database singleton
     */
    constructor(@inject(TYPES.Database) database: Database) {
        super();
        this.model = database.logs;
    }
}
