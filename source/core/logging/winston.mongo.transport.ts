import {Transport, TransportOptions} from 'winston';

import {ILogDocument} from './log.document';
import {ILogger} from './logger.interface';
import {LoggerFactory} from './logger.factory';
import {LogRepository} from './log.repository';

/**
 * Winston transport to store logs in the MongoDB.
 */
export class WinstonMongoTransport extends Transport {
    /** Marker to log only the first error */
    private isError: boolean;

    /** Logger instance. Must not be static. */
    private logger: ILogger = LoggerFactory.getLogger(__filename);

    /**
     * Ctor.
     * @param logRepository The log repostiory for storing logs
     * @param options       Winston Transport options
     */
    constructor(
        private logRepository: LogRepository,
        options?: TransportOptions) {
        super(options);
    }

    /**
     * The log function which is called by Winston.
     * @param level     Level of the log
     * @param message   Message of the log
     * @param metadata  Optional metadata
     * @param callback  unused callback
     */
    public log(level: string, message: string, metadata: any, callback: any) {
        const doc: ILogDocument = <ILogDocument> {
            level:      level,
            message:    message,
            metadata:   metadata,
            timestamp:  new Date()

        };
        this.logRepository
            .create(doc)
            .catch((e) => {
                // in case of error log the first occurance only.
                if (!this.isError) {
                    this.isError = true;
                    this.logger.warn(`Could not save the log in MongoDB: ${e.message}`);
                }
            });
    }
}

