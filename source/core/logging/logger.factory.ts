import {LoggerInstance} from 'winston';
import * as winston from 'winston';

import {config} from '../config/config';
import {ILogger} from './logger.interface';
import {Logger} from './logger';
import {LogRepository} from './log.repository';
import {WinstonMongoTransport} from './winston.mongo.transport';

/**
 * Inits and construct lightweighted loggers.
 * Implements the ServiceLocator Pattern.
 */
export class LoggerFactory {
    /**
     * Our one and only single instance of the heavyweighted winston logger.
     * Initialized with the winston default logger. Therefore the logging output
     * changes after the initialization is performed.
     */
    private static logger: LoggerInstance | null = null;

    /** Watcher for multiple initialisations */
    private static isInitialized: boolean = false;

    /**
     * The root dir of the node application. This is used to log only the relative path
     * and must be initialized prior each logger creation.
     */
    private static rootDir: string = '';

    /**
     * Getter for the heavyweighted winston logger. Only for internal use
     * (to optimize memory consumption of the lightweighted logger instances).
     * @returns {LoggerInstance}
     */
    public static getInternalLogger(): LoggerInstance {
        // Create our start logger for Console only.
        // Don't use the Winston default logger because it may be undefined
        // when used in unit tests.
        if (LoggerFactory.logger === null) {
            LoggerFactory.logger = new winston.Logger({
                transports: [new winston.transports.Console()]
            });
        }

        // return instance
        return LoggerFactory.logger;
    }

    /**
     * Initializes the logger for production environments.
     * Two transports are provided (Console and MongoDB). MongoDB logging is extremely
     * expensiv, but comes with no cost for Heroku. May be changed in future.
     * @param core Connected MongoDB
     */
    public static initLogger(logRepository: LogRepository) {
        // shield against multiple calls
        if (LoggerFactory.isInitialized) {
            throw new Error('LoggerFactory must only ininitalized once.');
        }
        LoggerFactory.isInitialized = true;

        // create our logger
        LoggerFactory.logger = new (winston.Logger)({
            emitErrs:       false,
            exitOnError:    false,
            level:          config.logging.level,
            transports: [
                new winston.transports.Console({
                    colorize:       config.logging.colorize,
                    level:          'info',
                    prettyPrint:    true,
                    silent:         false,
                    timestamp:      true
                }),
                new WinstonMongoTransport(logRepository, {
                    level:          'debug'
                })
            ]
        });
    }

    /**
     * Creates a lightweighted logger instance.
     * @param modulePath    Path of the module to log
     * @returns {ILogger}
     */
    public static getLogger(modulePath: string): ILogger {
        if (LoggerFactory.rootDir === '') {
            LoggerFactory.detectRootDir();
        }
        return new Logger(modulePath.replace(LoggerFactory.rootDir, ''));
    }

    /**
     * Creates an adapter for Morgan logging.
     * This is used for routing all Morgan logs over Winston.
     * @param log The logger used for routing.
     * @returns {{write: ((message:any, encoding:any)=>undefined)}}
     */
    public static getMorganStreamAdapter(log: ILogger): any {
        return {
            write: function (message: string, encoding: string) {
                log.info(message.slice(0, -1));
            }
        };
    }

    /**
     * Sets the root directory based on the location of this class.
     */
    private static detectRootDir(): void {
        const lastIndex = __dirname.lastIndexOf('core/logging');
        if (lastIndex === -1) {
            throw new Error('Wrong path setting in function LoggerFactory.detectRootDir');
        }
        LoggerFactory.rootDir = __dirname.substring(0, lastIndex);
    }

    /** Private ctor to prevent construction. */
    private constructor() {
    }
}
