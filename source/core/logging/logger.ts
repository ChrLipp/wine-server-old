import {ILogger} from './logger.interface';
import {LoggerFactory} from './logger.factory';

/**
 * Lightweigth wrapper over our singleton winston instance.
 */
export class Logger implements ILogger {
    /**
     * Ctor.
     * @param module Module to log.
     */
    constructor(private module: string) {
        // if (this.module.length < Logger.moduleLength) {
        //     this.module = new Array(Logger.moduleLength - module.length + 1).join(' ') + this.module;
        // }
    }

    /**
     * Log in debug level.
     * @param msg   Message to log
     * @param meta  Metainfo to log
     */
    public debug(msg: string, ...meta: any[]): void {
        LoggerFactory.getInternalLogger().log('debug', `${this.buildModuleString()}${msg}`, meta);
    }

    /**
     * Log in verbose level.
     * @param msg   Message to log
     * @param meta  Metainfo to log
     */
    public verbose(msg: string, ...meta: any[]): void {
        LoggerFactory.getInternalLogger().log('verbose', `${this.buildModuleString()}${msg}`, meta);
    }

    /**
     * Log in info level.
     * @param msg   Message to log
     * @param meta  Metainfo to log
     */
    public info(msg: string, ...meta: any[]): void {
        LoggerFactory.getInternalLogger().log('info', `${this.buildModuleString()}${msg}`, meta);
    }

    /**
     * Log in warn level.
     * @param msg   Message to log
     * @param meta  Metainfo to log
     */
    public warn(msg: string, ...meta: any[]): void {
        LoggerFactory.getInternalLogger().log('warn', `${this.buildModuleString()}${msg}`, meta);
    }

    /**
     * Log in error level.
     * @param msg   Message to log
     * @param meta  Metainfo to log
     */
    public error(msg: string, ...meta: any[]): void {
        LoggerFactory.getInternalLogger().log('error', `${this.buildModuleString()}${msg}`, meta);
    }

    /**
     * Build the module string.
     * @returns {string}
     */
    private buildModuleString(): string {
        return `[${this.module}] `;
    }
}

