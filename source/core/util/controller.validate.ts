import {Request} from 'express';
import {Deserialize} from 'cerialize';
import {validate, Validator, ValidationError} from 'class-validator';

import {ValidationException} from '../error/exception/validation.exception';

/**
 * Validates a request body based on the given object validation annotations.
 * @param request   Request
 * @param target    Object which contains the validation annotation
 * @returns {Promise<Tstring>}
 */
export function validateBody(request: Request, target: any): any {
    const toValidate = Deserialize(request.body, target);
    return validate(toValidate)
        .then((errors) => {
            if (errors.length > 0) {
                const validationMsg: string = errors
                    .map((currentValue: ValidationError, index, array) => {
                        return `'${currentValue.property}'`;
                    })
                    .join();
                throw new ValidationException(`Column(s) ${validationMsg} failed validation.`);
            }
            else {
                return toValidate;
            }
        });
}

/**
 * Validates a MongoDB ID.
 * @param id MongoDB ID
 * @returns {string} the id for chaining
 */
export function validateMongoId(id: string) {
    const validator = new Validator();
    if (!validator.isMongoId(id)) {
        throw new ValidationException(`ID '${id}' failed validation.`);
    }
    return id;
}
