import {inject, injectable} from 'inversify';
import {Profile} from 'passport';

import {AbstractStrategyFactory} from './abstract.strategy.factory';
import {IUserDocument, UserDocument, ILastLogin} from '../../modules/user/model/user.document';
import {TYPES} from '../../ioc/types';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Base class for passport OAuth strategies.
 */
@injectable()
export abstract class AbstractOAuthStrategyFactory extends AbstractStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Login or register a Facebook user.
     * @param accessToken   Provider access token
     * @param profile       Provider profile
     * @returns {Promise<UserDocument>} Created or read user
     */
    protected async login(accessToken: string, profile: Profile): Promise<UserDocument | null> {
        // first, search for the unique Provider ID
        let entry = await this.userRepository.findOne(this.getQueryForProviderId(profile.id));

        if (!entry) {
            // if not found the user might be registered with a local user or
            // with another social network. In this case update the user with the
            // Provider information when his email is present
            if (profile.emails) {
                entry = await this.userRepository.findOne({
                    'email': profile.emails[0].value
                });
                if (entry) {
                    await this.updateLoginInfo(entry._id, profile);
                }
            }
            if (!entry) {
                // A user with the email adress couldn't be found,
                // therefor we have a new entry, so create/register the user
                if (profile.emails && profile.emails[0]) {
                    let newEntry: IUserDocument = {
                        email:      profile.emails[0].value,
                        lastLogin:  new Date(),
                        name:       profile.displayName
                    };
                    this.buildProviderEntry(newEntry, profile);
                    entry = await this.userRepository.create(newEntry);
                }
                else {
                    return null;
                }
            }
        }
        else {
            await this.updateLoginInfo(entry._id, profile);
        }
        return entry;
    }

    /**
     * Returns a query object for retrieving a provider profile by id.
     * @param id   ID of the profile
     */
    protected abstract getQueryForProviderId(id: string): any;

    /**
     * Updates the login info (id, token and last login) for the given provider.
     * Empty implementation.
     * @param id            The id of user for the given provider
     * @param profile       The userprofile for the given provider
     */
    private async updateLoginInfo(id: string, profile: Profile): Promise<void> {
        let updateEntry: ILastLogin = {
            lastLogin:  new Date()
        };
        this.buildProviderEntry(updateEntry, profile);
        return this.userRepository.update(id, updateEntry);
    }

    /**
     * Returns the provider key for storing it in the Mongo.
     * @returns {string}
     */
    protected abstract getProviderKey(): string;

    /**
     * Build the data structure for storing a provider entry.
     * @param entry     Entry to update with the provider entry
     * @param profile   Provider profile of the user
     */
    private buildProviderEntry(entry: ILastLogin, profile: Profile): void {
        entry[this.getProviderKey()] = {
            id:     profile.id
        };
    }
}
