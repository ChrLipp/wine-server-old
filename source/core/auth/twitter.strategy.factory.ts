import {inject, injectable} from 'inversify';
import {IStrategyOption, Strategy, Profile} from 'passport-twitter';

import {AbstractOAuthStrategyFactory} from './abstract.oauth.strategy.factory';
import {config} from '../config/config';
import {TYPES} from '../../ioc/types';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Strategy for passport-twitter.
 */
@injectable()
export class TwitterStrategyFactory extends AbstractOAuthStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Creates the Twitter strategy.
     * @returns {Strategy}
     */
    public create(): Strategy {
        const twitterOptions: IStrategyOption = {
            callbackURL:    `${config.server.url}api/user/twittercallback`,
            consumerKey:    config.auth.twitter.consumerId,
            consumerSecret: config.auth.twitter.consumerSecret,
            includeEmail:   true
        };

        return new Strategy(
            twitterOptions,
            async (accessToken, refreshToken, profile: Profile, done): Promise<void> => {
                try {
                    const user = await this.login(accessToken, profile);
                    this.returnUser(done, user);
                }
                catch (error) {
                    done(error, null);
                }
            });
    }

    /**
     * Returns a query object for retrieving a provider profile by id.
     * @param id   ID of the profile
     * @returns {null}
     */
    protected getQueryForProviderId(id: string): any {
        return {
            'twitterUser.id': id
        };
    }

    /**
     * Returns the provider key for storing it in the Mongo.
     * @returns {string}
     */
    protected getProviderKey(): string {
        return 'twitterUser';
    }
}
