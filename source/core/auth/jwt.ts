import {sign} from 'jsonwebtoken';

import {config} from '../config/config';
import {IUserDocument} from '../../modules/user/model/user.document';

export interface IJwtContent {
    exp?:   Date;
    iat?:   Date;

    /** ID of the user */
    userId: string;

    /** Name of the user */
    name: string;

    /** EMail of the user */
    email: string;
}

export function generateJwt(user: IUserDocument): string {
    return sign({
        email:  user.email,
        name:   user.name,
        userId: user._id
    } as IJwtContent, config.auth.local.secret, { expiresIn: config.auth.local.expiresIn });

}
