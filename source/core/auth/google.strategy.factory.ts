import {inject, injectable} from 'inversify';
import {Profile} from 'passport';
import {Strategy} from 'passport-google-oauth20';

import {AbstractOAuthStrategyFactory} from './abstract.oauth.strategy.factory';
import {config} from '../config/config';
import {TYPES} from '../../ioc/types';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Strategy for passport-google-oauth20.
 */
@injectable()
export class GoogleStrategyFactory extends AbstractOAuthStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Creates the Google strategy.
     * @returns {Strategy}
     */
    public create(): Strategy {
        const goggleOptions = {
            callbackURL:    `${config.server.url}api/user/googlecallback`,
            clientID:       config.auth.google.clientId,
            clientSecret:   config.auth.google.clientSecret,
            scope:          ['profile', 'email']
        };

        return new Strategy(
            goggleOptions,
            async (accessToken: string, refreshToken: string, profile: Profile, done: any): Promise<void> => {
                try {
                    const user = await this.login(accessToken, profile);
                    this.returnUser(done, user);
                }
                catch (error) {
                    done(error, null);
                }
            });
    }

    /**
     * Returns a query object for retrieving a provider profile by id.
     * @param id   ID of the profile
     * @returns {null}
     */
    protected getQueryForProviderId(id: string): any {
        return {
            'googleUser.id': id
        };
    }

    /**
     * Returns the provider key for storing it in the Mongo.
     * @returns {string}
     */
    protected getProviderKey(): string {
        return 'googleUser';
    }
}
