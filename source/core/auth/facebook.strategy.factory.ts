import {inject, injectable} from 'inversify';
import {IStrategyOption, Strategy, Profile} from 'passport-facebook';

import {AbstractOAuthStrategyFactory} from './abstract.oauth.strategy.factory';
import {config} from '../config/config';
import {TYPES} from '../../ioc/types';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Strategy for passport-facebook.
 */
@injectable()
export class FacebookStrategyFactory extends AbstractOAuthStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Creates the Facebook strategy.
     * @returns {Strategy}
     */
    public create(): Strategy {
        const facebookOptions: IStrategyOption = {
            callbackURL:    `${config.server.url}api/user/fbcallback`,
            clientID:       config.auth.facebook.appId,
            clientSecret:   config.auth.facebook.appSecret,
            enableProof:    true,
            profileFields:  ['id', 'displayName', 'name', 'emails']
        };

        return new Strategy(
            facebookOptions,
            async (accessToken, refreshToken, profile: Profile, done): Promise<void> => {
                try {
                    const user = await this.login(accessToken, profile);
                    this.returnUser(done, user);
                }
                catch (error) {
                    done(error, null);
                }
            });
    }

    /**
     * Returns a query object for retrieving a provider profile by id.
     * @param id   ID of the profile
     * @returns {null}
     */
    protected getQueryForProviderId(id: string): any {
        return {
            'facebookUser.id': id
        };
    }

    /**
     * Returns the provider key for storing it in the Mongo.
     * @returns {string}
     */
    protected getProviderKey(): string {
        return 'facebookUser';
    }
}
