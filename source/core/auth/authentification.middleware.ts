import {injectable, multiInject} from 'inversify';
import {Handler} from 'express';
import * as passport from 'passport';

import {AbstractStrategyFactory} from './abstract.strategy.factory';
import {TYPES} from '../../ioc/types';

/**
 * Authentification middleware.
 */
@injectable()
export class AuthentificationMiddleware {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(
        @multiInject(TYPES.StrategyFactory) private strategies: AbstractStrategyFactory[]) {
    }

    /**
     * Return the initialized passport handler.
     * @returns {express.Handler}
     */
    public init(): Handler {
        for (let strategy of this.strategies) {
            passport.use(strategy.create());
        }
        return passport.initialize();
    }
}
