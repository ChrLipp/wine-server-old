import * as bluebird from 'bluebird';
import * as crypto from 'crypto';

const pbkdf2 = bluebird.promisify(crypto.pbkdf2);

/**
 * Hashes the password. Since this is the centralized hash function,
 * the settings are hardcoded.
 * @param password  Password to hash
 * @param salt      Salt for hashing (Buffer)
 * @returns {Promise<string>} Hashed Password as hex string
 */
export async function hashPassword(password: string, salt: Buffer): Promise<string> {
    const hashedPassword = await pbkdf2(password, salt, 10000, 512, 'sha512');
    return hashedPassword.toString('hex');
}
