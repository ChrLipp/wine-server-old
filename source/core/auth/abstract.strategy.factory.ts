import {injectable} from 'inversify';
import {Strategy} from 'passport';

import {UserDocument} from '../../modules/user/model/user.document';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Base class for passport strategies.
 */
@injectable()
export abstract class AbstractStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(protected userRepository: UserRepository) {
    }

    /**
     * Returns the user to the passport module.
     * @param done  Passport done callback
     * @param user  User document
     */
    protected returnUser(done: any, user: UserDocument | null): void {
        if (user !== null) {
            done(null, user);
        }
        else {
            done(null, false, 'Wrong username/password.');
        }
    }

    /**
     * Creates the strategy.
     */
    public abstract create(): Strategy;
}
