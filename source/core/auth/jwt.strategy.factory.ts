import {inject, injectable} from 'inversify';
import {ExtractJwt, Strategy, StrategyOptions} from 'passport-jwt';

import {AbstractStrategyFactory} from './abstract.strategy.factory';
import {config} from '../config/config';
import {IJwtContent} from './jwt';
import {TYPES} from '../../ioc/types';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Strategy for passport-jwt.
 */
@injectable()
export class JwtStrategyFactory extends AbstractStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Creates the JWT strategy.
     * @returns {Strategy}
     */
    public create(): Strategy {
        const jwtOptions: StrategyOptions = {
            jwtFromRequest: ExtractJwt.fromAuthHeader(),
            secretOrKey:    config.auth.local.secret
        };

        return new Strategy(jwtOptions,
            async (jwtPayload: IJwtContent, done: any): Promise<void> => {
                if (jwtPayload.exp) {
                    jwtPayload.exp = new Date(Number(jwtPayload.exp) * 1000);
                }
                if (jwtPayload.iat) {
                    jwtPayload.iat = new Date(Number(jwtPayload.iat) * 1000);
                }
                done(null, jwtPayload);
            });
    }
}
