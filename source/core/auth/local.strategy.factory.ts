import {inject, injectable} from 'inversify';
import {Strategy} from 'passport-local';

import {AbstractStrategyFactory} from './abstract.strategy.factory';
import {hashPassword} from './hash';
import {TYPES} from '../../ioc/types';
import {UserDocument} from '../../modules/user/model/user.document';
import {UserRepository} from '../../modules/user/repository/user.repository';

/**
 * Strategy for passport-local.
 */
@injectable()
export class LocalStrategyFactory extends AbstractStrategyFactory {
    /**
     * Ctor.
     * @param storageSpaceRepository The storage space repository
     */
    constructor(@inject(TYPES.UserRepository) userRepository: UserRepository) {
        super(userRepository);
    }

    /**
     * Creates the local strategy.
     * @returns {Strategy}
     */
    public create(): Strategy {
        return new Strategy(
            async (username: string, password: string, done: any): Promise<void> => {
                try {
                    const user = await this.login(username, password);
                    this.returnUser(done, user);
                }
                catch (error) {
                    done(error, null);
                }
            });
    }

    /**
     * Performs the login.
     * @param username
     * @param password
     * @returns {Promise<IUserDocument | null>}
     */
    private async login(username: string, password: string): Promise<UserDocument | null> {
        // verify password
        const entry = await this.userRepository.findOne({
            'email': username
        });
        if (!entry) {
            return null;
        }
        const hashedPassword = await hashPassword(
            password, Buffer.from(entry.localUser.salt, 'hex'));
        if (hashedPassword !== entry.localUser.hashedPassword) {
            return null;
        }

        // store login and return user
        await this.userRepository.update(entry._id, {lastLogin: new Date()});
        return entry;
    }
}
