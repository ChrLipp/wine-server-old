import {loadEnv, enforceMandatoryEntries, validate} from './config.loader';

export function loadAndValidateConfig() {
    // load env settings
    loadEnv();

    // check if all mandatory entries are set
    enforceMandatoryEntries([
        'NODE_ENV',
        'AUTH_SECRET', 'SESSION_SECRET',
        'FACEBOOK_APP_ID', 'FACEBOOK_APP_SECRET',
        'GOOGLE_CLIENT_ID', 'GOOGLE_CLIENT_SECRET',
        'MONGODB_URI',
        'MAILGUN_API_KEY', 'MAILGUN_DOMAIN',
        'SMTP_LOGIN', 'SMTP_PASSWORD', 'SMTP_SERVER',
        'TWITTER_CONSUMER_KEY', 'TWITTER_CONSUMER_SECRET',
        'APP_URL'
    ]);

    // validate elements others than plain strings
    validate([
        {
            name: 'ALLOW_TEMPORARY_MAILS',
            valCb: (v, value) => {
                return v.isBooleanString(value);
            }
        }, {
            name: 'APP_URL',
            valCb: (v, value) => {
                return v.matches(value, /^http[s]?:\/\/([\w\.\-]+)(:\d+)?\/$/);
            }
        }, {
            name: 'COLORIZE',
            valCb: (v, value) => {
                return v.isBooleanString(value);
            }
        }, {
            name: 'CONFIRM_VALIDITY',
            valCb: (v, value) => {
                return v.isNumberString(value);
            }
        }, {
            name: 'LOG_LEVEL',
            valCb: (v, value) => {
                return v.isIn(value, ['debug', 'verbose', 'info', 'warn', 'error']);
            }
        }, {
            name: 'MAILGUN_SMTP_PORT',
            valCb: (v, value) => {
                return v.isNumberString(value);
            }
        }, {
            name: 'MONGODB_URI',
            valCb: (v, value) => {
                return v.matches(value, /mongodb:\/\/.*/);
            }
        }, {
            name: 'NODE_ENV',
            valCb: (v, value) => {
                return v.isIn(value, ['development', 'production', 'test']);
            }
        }, {
            name: 'PORT',
            valCb: (v, value) => {
                return v.isNumberString(value) && v.min(+value, 1024);
            }
        }, {
            name: 'SMPT_PORT',
            valCb: (v, value) => {
                return v.isNumberString(value);
            }
        }
    ]);
}

