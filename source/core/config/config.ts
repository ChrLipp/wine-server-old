import {loadAndValidateConfig} from './config.validate';
loadAndValidateConfig();

// Define and export config. Default values are defined here.
// When no default value is given, then the env var should
// be listed in the mandatory entries list in the config.validate.ts file.
export const config: any = {
    auth: {
        facebook: {
            appId:              process.env.FACEBOOK_APP_ID,
            appSecret:          process.env.FACEBOOK_APP_SECRET
        },
        google: {
            clientId:           process.env.GOOGLE_CLIENT_ID,
            clientSecret:       process.env.GOOGLE_CLIENT_SECRET,
        },
        local: {
            expiresIn:          86400,
            secret:             process.env.AUTH_SECRET,
        },
        oauth: {
            errorUrl:           'api/health',
            sessionSecret:      process.env.SESSION_SECRET,
            sucessUrl:          'api/health'
        },
        twitter: {
            consumerId:         process.env.TWITTER_CONSUMER_KEY,
            consumerSecret:     process.env.TWITTER_CONSUMER_SECRET
        }
    },
    environment: {
        isDev:                  process.env.NODE_ENV === 'development',
        isProd:                 process.env.NODE_ENV === 'production',
        isTest:                 process.env.NODE_ENV === 'test',
        name:                   process.env.NODE_ENV
    },
    logging: {
        colorize:               (process.env.COLORIZE || 'false') === 'true',
        level:                  process.env.LOG_LEVEL || 'debug',
    },
    mail: {
        allowTempMails:         (process.env.ALLOW_TEMPORARY_MAILS || 'false') === 'true',
        mailgun: {
            apiKey:             process.env.MAILGUN_API_KEY,
            domain:             process.env.MAILGUN_DOMAIN,
        },
        registration: {
            // the confirmation link is build with server.url + mail.registration.confirmLink + id
            confirmLink:        'api/user/confirm/',
            // Validiy of the confirmation link in minutes or 0 forever (current 14 days)
            // If you change this value, you have to delete the collection 'User',
            // then compile and restart the application
            confirmValidity:    Number(process.env.CONFIRM_VALIDITY || 20160)
        },
        smtp: {
            password:           process.env.SMTP_PASSWORD,
            port:               Number(process.env.SMTP_PORT || 587),
            server:             process.env.SMTP_SERVER,
            user:               process.env.SMTP_LOGIN
        }
    },
    mongo: {
        uri:                    process.env.MONGODB_URI
    },
    server: {
        port:                   Number(process.env.PORT || 3000),
        url:                    process.env.APP_URL
    }
};
