/* tslint:disable:no-var-requires */
const stubTransport = require('nodemailer-stub-transport');
/* tslint:enable:no-var-requires */
import {Transport} from 'nodemailer';

export function getStubTransport(): Transport {
    return stubTransport();
}
