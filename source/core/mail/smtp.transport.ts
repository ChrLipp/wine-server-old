import * as smtpTransport from 'nodemailer-smtp-transport';
import {Transport} from 'nodemailer';

import {config} from '../../core/config/config';

export function getSmtpTransport(): Transport {
    return smtpTransport({
        auth: {
            pass:           config.mail.smtp.password,
            user:           config.mail.smtp.user,
        },
        host:               config.mail.smtp.server,
        port:               config.mail.smtp.port
    });
}
