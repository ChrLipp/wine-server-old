import {Transport} from 'nodemailer';
/* tslint:disable:no-var-requires */
const mailgunTransport = require('nodemailer-mailgun-transport');
/* tslint:enable:no-var-requires */

import {config} from '../../core/config/config';

export function getMailgunTransport(): Transport {
    return mailgunTransport({
        auth: {
            api_key:    config.mail.mailgun.apiKey,
            domain:     config.mail.mailgun.domain
        }
    });
}
