import {inject, injectable} from 'inversify';
import {Transport, SentMessageInfo, Transporter, SendMailOptions} from 'nodemailer';
import * as bluebird from 'bluebird';
import * as fs from 'fs';
import * as handlebars from 'handlebars';
import * as nodemailer from 'nodemailer';
import * as path from 'path';

import {TYPES} from '../../ioc/types';

const fsAsync: any = bluebird.promisifyAll(fs);

export interface IMailOptions {
    from:           string;
    params:         any;
    to:             string;
    subject:        string;
    textBody?:      string;
    templatePath?:  string;
}

/**
 * Mail service.
 * The underlying mail infrastructure is defined by the given transport.
 */
@injectable()
export class MailService {
    /** Encapsulates the mail infrastructure */
    private transporter: Transporter;

    /**
     * Ctor.
     * @param transportFactory Encapsulates the mail infrastructure
     */
    constructor(
        @inject(TYPES.FactoryTransport) transportFactory: () => Transport) {
        this.transporter = nodemailer.createTransport(transportFactory());
    }

    public async send(options: IMailOptions): Promise<SentMessageInfo> {
        const params: SendMailOptions = {
            from:       'DoNotReply <sender@server.com>',
            subject:    options.subject,
            to:         options.to
        };

        if (options.templatePath) {
            let callback = await this.compileTemplate(options.templatePath);
            params.html = callback(options.params);
        }
        else if (options.textBody) {
            params.text = options.textBody;
        }

        return this.transporter.sendMail(params);
    }

    private async compileTemplate(templatePath: string): Promise<HandlebarsTemplateDelegate> {
        const content: string = await fsAsync.readFileAsync(path.normalize(templatePath), { encoding: 'utf8' });
        return handlebars.compile(content);
    }
}
