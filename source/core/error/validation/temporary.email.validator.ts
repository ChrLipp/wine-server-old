import {ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from 'class-validator';

/* tslint:disable:no-var-requires */
const domains = require('disposable-email-domains');
/* tslint:enable:no-var-requires */

import {config} from '../../config/config';

/**
 * Validation foar temporary email provider.
 */
@ValidatorConstraint({ name: 'temporaryEmail', async: false })
export class TemporaryEmailValidator implements ValidatorConstraintInterface {
    /**
     * Method to be called to perform custom validation over given value.
     * For async validations you must return a Promise<boolean> here.
     */
    public validate(email: string, args: ValidationArguments): boolean {
        if (config.mail.allowTempMails) {
            return true;
        }
        else {
            const domain = email.toLowerCase().split('@')[1];
            return !domains.includes(domain);
        }
    }

    /**
     * Gets default message when validation for this constraint fail.
     * @param args
     * @returns {string}
     */
    public defaultMessage(args: ValidationArguments): string {
        return `Temporary mail adresses (${args.value}) are not allowed!`;
    }
}
