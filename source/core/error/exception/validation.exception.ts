import {BaseException} from './base.exception';

/**
 * Exception when an item is not found.
 */
export class ValidationException extends BaseException {
    /**
     * Ctor.
     * @param message Error message
     */
    constructor(message: string) {
        super(message);
        this.name = 'ValidationException';
        this.statusCode = 400;
    }
}
