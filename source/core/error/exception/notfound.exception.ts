import {BaseException} from './base.exception';

/**
 * Exception when an item is not found.
 */
export class NotFoundException extends BaseException {
    /**
     * Ctor.
     * @param message Error message
     */
    constructor(message: string) {
        super(message);
        this.name = 'NotFoundException';
        this.statusCode = 404;
    }
}
