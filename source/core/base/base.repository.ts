import {injectable} from 'inversify';
import {Instance, Model, toObjectID} from 'iridium';

import {NotFoundException} from '../error/exception/notfound.exception';

/**
 * Abstract base repository for easy repository creating.
 */
@injectable()
export abstract class BaseRepository<U, T extends Instance<U, T>> {
    /** Collection model, must be set in the successor */
    protected model: Model<U, T>;

    /**
     * Inserts an object into the collection after validating it against this model's schema.
     * @param   item The item to insert into the collection
     * @returns {Promise<T>}
     */
    public create(item: U): Promise<any> {
        return Promise.resolve(this.model.insert(item));
    }

    /**
     * Returns all documents in the collection which match the conditions or null.
     * @param   conditions The MongoDB query dictating which documents to return
     * @returns {Promise<any>}
     */
    public find(conditions?: any): Promise<any> {
        return Promise.resolve(this.model.find(conditions));
    }

    /**
     * Returns the document in the collection which match the conditions or null.
     * @param   conditions The MongoDB query dictating which document to return
     * @returns {Promise<T>}
     */
    public findOne(conditions?: any): Promise<any> {
        return Promise.resolve(this.model.findOne(conditions));
    }

    /**
     * Returns the document in the collection with the given id.
     * Throws an exception if not found.
     * @param   id The given id identifiying the document to return
     * @returns {Promise<T>}
     */
    public findById(_id: string): Promise<any> {
        return Promise
            .resolve(this.model.findOne(_id))
            .then((item) => {
                if (item === null) {
                    throw new NotFoundException(`Item with ${_id} not found.`);
                }
                return item;
            });
    }

    /**
     * Updates the document in the collection with the given id.
     * Throws an exception if not found.
     * @param   _id The given id identifiying the document to delete
     * @param   item The item to insert into the collection
     * @returns {Promise<number>} number of updated documents
     */
    public update(_id: string, item: any): Promise<void> {
        // convert string based id in Object ID
        if (item._id) {
            item._id = toObjectID(item._id);
        }

        // perform the update
        return Promise
            .resolve(this.model.update({ _id: _id }, { $set: item }))
            .then((count) => {
                if (count === 0) {
                    throw new NotFoundException(`Item with ${_id} not found.`);
                }
            });
    }

    /**
     * Deletes the document in the collection with the given id.
     * Throws an exception if not found.
     * @param   _id The given id identifiying the document to delete
     * @returns {Promise<T>} number of deleted rows
     */
    public delete(_id: string): Promise<void> {
        return Promise
            .resolve(this.model.remove(_id))
            .then((count) => {
                if (count === 0) {
                    throw new NotFoundException(`Item with ${_id} not found.`);
                }
            });
    }
}
