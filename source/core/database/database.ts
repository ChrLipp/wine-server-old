import {inject, injectable} from 'inversify';
import {Core, Model, Configuration} from 'iridium';
import * as Bluebird from 'bluebird';
import * as log from 'winston';

import {ILogDocument, LogDocument} from '../logging/log.document';
import {IStorageSpaceDocument, StorageSpaceDocument} from '../../modules/storage.space/model/storage.space.document';
import {IUserDocument, UserDocument} from '../../modules/user/model/user.document';
import {TYPES} from '../../ioc/types';

/**
 * Provides MongoDB functionality, lists all collections.
 */
@injectable()
export class Database extends Core {

    /** Log Collection */
    public logs: Model<ILogDocument, LogDocument> =
        new Model<ILogDocument, LogDocument>(this, LogDocument);

    /** StorageSpace Collection */
    public storageSpaces: Model<IStorageSpaceDocument, StorageSpaceDocument> =
        new Model<IStorageSpaceDocument, StorageSpaceDocument>(this, StorageSpaceDocument);

    /** User Collection */
    public users: Model<IUserDocument, UserDocument> =
        new Model<IUserDocument, UserDocument>(this, UserDocument);

    /**
     * Ctor.
     * @param uri       Connection URL
     * @param config    Optional database configuration
     */
    constructor(
        @inject(TYPES.DatabaseUrl) uri: string,
        @inject(TYPES.DatabaseConfig) config?: Configuration) {
        super(uri, config);
        log.info(`Connecting to ${uri}`);
    }

    /**
     * onConnected callback: initializes all indexes.
     * @returns {Bluebird<U>}
     */
    protected onConnected(): Bluebird<void> {
        return Bluebird
            .all([
                this.logs.ensureIndexes(),
                this.storageSpaces.ensureIndexes(),
                this.users.ensureIndexes()
            ])
            .then(() => {});
    }
}
