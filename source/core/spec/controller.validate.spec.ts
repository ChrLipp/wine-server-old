import {expect} from 'chai';

import {ValidationException} from '../error/exception/validation.exception';
import {validateMongoId} from '../util/controller.validate';

describe('Controller validation utils', () => {
    it('validates an invalid ID', () => {
        expect(() => {
            validateMongoId('1');
        }).to.throw(ValidationException, "ID '1' failed validation.");
    });

    it('validates a valid ID', () => {
        const id: string = '581a1745571e150010ae2872';
        expect(validateMongoId(id)).to.equal(id);
    });
});
