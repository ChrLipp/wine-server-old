import {expect} from 'chai';
import {ValidationArguments} from 'class-validator';

import {config} from '../config/config';
import {TemporaryEmailValidator} from '../error/validation/temporary.email.validator';

describe('Temporary EMail validator', () => {
    it('validates a valid EMail (allow = false)', () => {
        config.mail.allowTempMails = false;
        const validator = new TemporaryEmailValidator();
        const result = validator.validate('a@b.com', {} as ValidationArguments);

        expect(result).to.be.true;
    });

    it('validates a valid EMail (allow = true)', () => {
        config.mail.allowTempMails = true;
        const validator = new TemporaryEmailValidator();
        const result = validator.validate('a@b.com', {} as ValidationArguments);

        expect(result).to.be.true;
    });

    it('validates an invalid EMail (allow = false)', () => {
        config.mail.allowTempMails = false;
        const validator = new TemporaryEmailValidator();
        const result = validator.validate('a@mailinator.com', {} as ValidationArguments);

        expect(result).to.be.false;
    });

    it('validates an invalid EMail (allow = true)', () => {
        config.mail.allowTempMails = true;
        const validator = new TemporaryEmailValidator();
        const result = validator.validate('a@mailinator.com', {} as ValidationArguments);

        expect(result).to.be.true;
    });

    it('returns validation error', () => {
        const validator = new TemporaryEmailValidator();
        const result = validator.defaultMessage({value: 'a@b.com'} as ValidationArguments);

        expect(result).to.equal('Temporary mail adresses (a@b.com) are not allowed!');
    });
});
