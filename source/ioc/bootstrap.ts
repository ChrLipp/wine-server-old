import 'reflect-metadata';
/* tslint:disable */
import {interfaces as express_interfaces, TYPE} from 'inversify-express-utils';
/* tslint:enable */
import {Core} from 'iridium';
import {decorate, injectable, interfaces, Container} from 'inversify';
import {Transport} from 'nodemailer';

import {Application} from '../application';
import {AuthentificationMiddleware} from '../core/auth/authentification.middleware';
import {config} from '../core/config/config';
import {Database} from '../core/database/database';
import {FacebookStrategyFactory} from '../core/auth/facebook.strategy.factory';
import {getSmtpTransport} from '../core/mail/smtp.transport';
import {getStubTransport} from '../core/mail/stub.transport';
// import {getMailgunTransport} from '../core/mail/mailgun.transport';
import {GoogleStrategyFactory} from '../core/auth/google.strategy.factory';
import {HealthCheckController} from '../modules/healthcheck/controller/healthcheck.controller';
import {JwtStrategyFactory} from '../core/auth/jwt.strategy.factory';
import {LocalStrategyFactory} from '../core/auth/local.strategy.factory';
import {LoggerFactory} from '../core/logging/logger.factory';
import {LogRepository} from '../core/logging/log.repository';
import {MailService} from '../core/mail/mail.service';
import {StorageSpaceController} from '../modules/storage.space/controller/storage.space.controller';
import {StorageSpaceService} from '../modules/storage.space/service/storage.space.service';
import {StorageSpaceRepository} from '../modules/storage.space/repository/storage.space.repository';
import {TwitterStrategyFactory} from '../core/auth/twitter.strategy.factory';
import {TYPES} from './types';
import {UserController} from '../modules/user/controller/user.controller';
import {UserRepository} from '../modules/user/repository/user.repository';
import {UserService} from '../modules/user/service/user.service';

const container = new Container();

/* Application */
container
    .bind<Application>(TYPES.Application)
    .to(Application)
    .inSingletonScope();

/* Controllers */
container
    .bind<express_interfaces.Controller>(TYPE.Controller)
    .to(HealthCheckController)
    .whenTargetNamed('HealthCheckController');

container
    .bind<express_interfaces.Controller>(TYPE.Controller)
    .to(UserController)
    .whenTargetNamed('UserController');

container
    .bind<express_interfaces.Controller>(TYPE.Controller)
    .to(StorageSpaceController)
    .whenTargetNamed('StorageSpaceController');

/* Services */
container
    .bind<StorageSpaceService>(TYPES.StorageSpaceService)
    .to(StorageSpaceService)
    .inSingletonScope();

container
    .bind<UserService>(TYPES.UserService)
    .to(UserService)
    .inSingletonScope();

container
    .bind<interfaces.Factory<Transport>>(TYPES.FactoryTransport)
    .toFactory<Transport>(() => {
        return (): any => {
            if (config.environment.isTest) {
                // do not send any mail when in integration test
                // (on unit test the mail sending is hopefully stubbed in the test)
                return getStubTransport();
            }
            else {
                return getSmtpTransport();
            }
        };
    });

container
    .bind<MailService>(TYPES.MailService)
    .to(MailService)
    .inSingletonScope();

/* Repositories */
container
    .bind<LogRepository>(TYPES.LogRepository)
    .to(LogRepository)
    .inSingletonScope();

container
    .bind<StorageSpaceRepository>(TYPES.StorageSpaceRepository)
    .to(StorageSpaceRepository)
    .inSingletonScope();

container
    .bind<UserRepository>(TYPES.UserRepository)
    .to(UserRepository)
    .inSingletonScope();

/* Middleware */
container
    .bind<LocalStrategyFactory>(TYPES.StrategyFactory)
    .to(LocalStrategyFactory)
    .inSingletonScope();

container
    .bind<JwtStrategyFactory>(TYPES.StrategyFactory)
    .to(JwtStrategyFactory)
    .inSingletonScope();

container
    .bind<FacebookStrategyFactory>(TYPES.StrategyFactory)
    .to(FacebookStrategyFactory)
    .inSingletonScope();

container
    .bind<TwitterStrategyFactory>(TYPES.StrategyFactory)
    .to(TwitterStrategyFactory)
    .inSingletonScope();

container
    .bind<GoogleStrategyFactory>(TYPES.StrategyFactory)
    .to(GoogleStrategyFactory)
    .inSingletonScope();

container
    .bind<AuthentificationMiddleware>(TYPES.AuthentificationMiddleware)
    .to(AuthentificationMiddleware)
    .inSingletonScope();

/* Database setup */
container
    .bind<string>(TYPES.DatabaseUrl)
    .toConstantValue(config.mongo.uri);

container
    .bind<string>(TYPES.DatabaseConfig)
    .toConstantValue('');

container
    .bind<Database>(TYPES.Database)
    .to(Database)
    .inSingletonScope();

decorate(injectable(), Core);

/* Logging */
const logRepository = <LogRepository> container.get(TYPES.LogRepository);
LoggerFactory.initLogger(logRepository);

export default container;
