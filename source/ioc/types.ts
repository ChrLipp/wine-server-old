/**
 * Identifier for DI.
 */
export const TYPES = {
    Application:                Symbol('Application'),
    AuthentificationMiddleware: Symbol('AuthentificationMiddleware'),

    Database:                   Symbol('Database'),
    DatabaseConfig:             Symbol('DatabaseConfig'),
    DatabaseUrl:                Symbol('DatabaseUrl'),

    FactoryTransport:           Symbol('Factory<Transport>'),

    LogRepository:              Symbol('LogRepository'),

    MailService:                Symbol('MailService'),

    StorageSpaceRepository:     Symbol('StorageSpaceRepository'),
    StorageSpaceService:        Symbol('StorageSpaceService'),
    StrategyFactory:            Symbol('StrategyFactory'),

    UserRepository:             Symbol('UserRepository'),
    UserService:                Symbol('UserService')
};
