# Setup test infrastructure: Implementation notes

## Provide an unit test infrastructure

Unit tests have the pattern `*.spec.ts`.

Libraries used

- [Mocha](https://github.com/mochajs/mocha) as test framework
- [Chai](https://github.com/chaijs/chai) for a BDD / TDD assertion library
- [Sinon](http://sinonjs.org) for test spies, stubs and mocks

## Provide an integration test infrastructure 

Integration tests have the pattern `*.ispec.ts`.

Libraries used

- [Supertest](https://github.com/visionmedia/supertest) as test framework

## Issues and support

Initially the healthcheck controller didn't return a Content-Type.
First I thought this is an issue of inversify-express-utils and filed the issue   
[specify JSON response](https://github.com/inversify/InversifyJS/issues/433).
On closer look it turned out that for a proper Content-Type express requires that
an object is returned.
