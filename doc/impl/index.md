# Table of content: implementation notes

- Setup project
    - [Setup directory structure and build with Gulp 4](01_gulp.md)
    - [Setup express server](02_express.md)
    - [Setup MongoDB](03_mongo.md)
    - [Setup Logging](04_logging.md)
    - [Setup Mapper](05_mapper.md)
    - [Setup test infrastructure](06_test.md)
- User management
	- [Configuration management](07_config.md)
	- [Registration with username / password](08_registration.md)
	- [Login with username / password](09_login.md)
	- [Login/Registration with Facebook OAuth](10_fb_oauth.md)
	- [Login/Registration with Twitter OAuth](11_twitter_oauth.md)
	- [Login/Registration with Google OAuth](12_google_oauth.md)
	- [REST interface documentation](13_rest_doc.md)
- Miscellanious notes
	- [Heroku](99_heroku.md)
	- [MongoDB](99_mongo.md)
