# Setup MongoDB: Implementation notes

## Mongo

The MongoDB addon choosen on Heroku is "mLab MongoDB :: Mongodb", because it offers free tryout.
It can be installed with

	heroku addons:create mongolab
	
Provided environment variables are

- MONGODB_URI

I developed the MongoDB access in little steps and tested each step in Heroku to ensure my
solution is working both locally and on Heroku. To achieve this I developed locally in a branch
and pushed this branch to Heroku with:

    git push heroku 03_Mongo:master

## Express

The following middleware was used to implement the Mongo setup:

- [bodyParser](https://github.com/expressjs/body-parser) for parsing request bodies


    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));


## Iridium

During the implementation it turned out that Iridium doesn't support updating a single document.
An [issue](https://github.com/SierraSoftworks/Iridium/issues/55) was opened to request this
feature.

However the update was implemented with some support from Iridium:

- [Update with set](https://github.com/SierraSoftworks/Iridium/issues/58)
- [Optional JSON field](https://github.com/SierraSoftworks/Iridium/issues/57)
- [Typings for cursor missing](https://github.com/SierraSoftworks/Iridium/issues/56)
