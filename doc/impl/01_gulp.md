# Setup directory structure and build with Gulp 4: Implementation notes
## Directory structure

- build     For temporary build files
- dist      Distribution directory
- source    Source file (Typescript) directory
- test      Test source file (Typescript) directory


## Gulp 4
### Typescript 2.x with ES2016 async/await

We use babel with a node6 preset to transform typescript.

- typescript
- gulp-sourcemaps
- gulp-typescript
- gulp-babel
- babel-preset-latest-node6

The typescript compiler is configured with `tsconfig.json`.


### Lint

For linting we need

- tslint
- gulp-tslint


## Extend the code
### Yarn

I used [`yarn`](https://yarnpkg.com/) as a npm replacement

    npm install -g yarn

Add a dependency

    yarn add [package-name]
    yarn add [package-name] --exact


Add a development dependency

    yarn add [tool] --dev --exact


### Typings

Since I am using `typescript` 2.x, you can install typings e.g.
 
    yarn add @types/lodash --exact

This is only necessary when the typings are not part of the dependency itself.

## Version and updates

I use strict versioning which is the guarantee for repeatable builds.
For detecting updates I am using [npm-check-updates](https://www.npmjs.com/package/npm-check-updates).
Install it with

    npm install -g npm-check-updates

To check if updates are available use `ncu`.




