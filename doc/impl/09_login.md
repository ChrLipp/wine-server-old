# Implement login with username / password: Implementation notes

## Passport

Passport works with strategies, created in `AuthentificationMiddleware`. Each strategy receives
options and a callback with the authentification credentials (username/password or JWT token in
my case). The result of the callback is a call of `done`. There are three possibilities:

- Case of an error: `done(error, null);`
- Denied authentification: `done(null, false);`
- Accepted authentification: `done(null, user)`

The returned `user` is injected in the request and can be accessed with `request.user`.

On the controller I can restrict my route e.g. with
 
    @Post('/', passport.authenticate('jwt', {session: false}))

The JWT contains all the user data I need - therefore an additional hit to MongoDB for retreiving
the user is not necessary. Otherwise I would have to read the user data for each request. 


## Async await and exceptions

The following code block (from `StorageSpaceService`) delegates to the `StorageSpaceRepository`
and in case of an error transforms the `MongoError` to a `ValidationException`.
However the catch clause is only executed when you call the `create` with `await`.
The `await` keyword checks the returned promise and in case of a rejected promise it delegates
to the catch clause. Without the `await` the function returns a rejected promise and the catch
clause is never called.

    public async createStorageSpace(storageSpaceRequest: StorageSpaceRequest):
    Promise<IStorageSpaceDocument> {
        try {
            return await this.storageSpaceRepository.create(storageSpaceRequest);
        }
        catch (error) {
            if (error instanceof MongoError) {
                throw new ValidationException(
                    'A storage space with the same name already exists.');
            }
            throw error;
        }
    }

