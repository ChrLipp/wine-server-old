# Implement registration with username / password: Implementation notes

## Registration

The registration request is just a plain 'create user data' request followed by
a mail with a confirmation link.

## Libraries used

The following libraries have been used:

- [nodemailer](https://github.com/nodemailer/nodemailer)
- [nodemailer-smtp-transport](https://github.com/nodemailer/nodemailer-smtp-transport)
- [nodemailer-mailgun-transport](https://github.com/orliesaurus/nodemailer-mailgun-transport)

Nodemailer is a one stop solution for sending emails. The used technical approach and protocol
for sending the mails are not part of the core library. The user has to include the right
transport.

## External mail provider

### Mailgun

The first choosen mail provider is mailgun:

	heroku addons:create mailgun:starter

Provided environment variables are

- Access via API (3 times faster than SMTP)
    - MAILGUN_API_KEY
    - MAILGUN_DOMAIN
- Key for email validation
    - MAILGUN_PUBLIC_KEY
- SMTP Access
    - MAILGUN_SMTP_LOGIN
    - MAILGUN_SMTP_PASSWORD
    - MAILGUN_SMTP_PORT
    - MAILGUN_SMTP_SERVER

Use `heroku addons:docs mailgun` to view documentation.

This creates a so called 'sandbox domain' which means that you are only allowed to send mails
to 'authorized recipients' - recipients who must be registered in advance to use them.
To really use the service you need to set mailgun as the mail server for a custom domain.
This makes mailgun unusable for smaller applications.

### SMTP

An alternative for apps without an own domain is the Google GMail SMTP server.
There are two ways to use the server
 
- [Less secure apps](https://support.google.com/accounts/answer/6010255?hl=en) or
- Two factor authentification and
[application specific password](https://security.google.com/settings/security/apppasswords)
 
## Template engine

[Handlebars](http://handlebarsjs.com/) is used as template engine for the email confirmation mails.
Normally, Handlebars should be integrated in express as a view engine e.g. with 
[express-handlebars](https://github.com/ericf/express-handlebars).
This library integrates well with nodemailer with
[nodemailer-express-handlebars](https://github.com/yads/nodemailer-express-handlebars) and
allows use of a integrated template cache. In the current implementation I didn't want to
pull in a view engine, so the current simple implementation does't use a template cache at all. 

## Issues and support

- [Comparing password fields](https://github.com/pleerock/class-validator/issues/54)
- [this.timeout() fails when using ES6's arrow functions](https://github.com/mochajs/mocha/issues/2018)
- Supertest doesn't support promises, so I added [supertest-as-promised](https://github.com/WhoopInc/supertest-as-promised) on top
- [Mocking JavaScript a little](https://janmolak.com/mocking-javascript-a-little-26efb5f7f52a#.qje9oj8ls)
- [Promises in JavaScript Unit Tests: the Definitive Guide](https://www.sitepoint.com/promises-in-javascript-unit-tests-the-definitive-guide/)

## Open issues

- Acceptance test: The validation errors list is delivered to the client and correspond to the
  entered data - missing is analysis (format and multilanguage) and implementation 