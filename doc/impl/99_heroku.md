To list all Heroku environment variables use

    $ heroku run printenv | sort
    
List all releases

    $ heroku releases
    Rel   Change          By                    When
    ----  --------------  --------------------  --------------
    v51  Deploy de6388    stephan@example.com   7 minutes ago
    v50  Deploy 7c35f77   stephan@example.com   3 hours ago

Rollback to the last release

    $ heroku rollback
    Rolled back to v50

Tail on your heroku logs

    $ heroku logs --tail
    Running printenv on x-server... up, run.6617 (Free)
    DYNO=run.6617
    HOME=/app
    MEMORY_AVAILABLE=512
    MONGODB_URI=mongodb://xxx
    NODE_ENV=production
    NODE_HOME=/app/.heroku/node
    PATH=/app/.heroku/node/bin:/usr/local/bin:/usr/bin:/bin:/app/bin:/app/node_modules/.bin
    PORT=54066
    PS1=\[\033[01;34m\]\w\[\033[00m\] \[\033[01;32m\]$ \[\033[00m\]
    PWD=/app
    SHLVL=1
    TERM=xterm-256color
    WEB_CONCURRENCY=1
    WEB_MEMORY=512
    _=/usr/bin/printenv
    
Provide the [application URL](http://stackoverflow.com/questions/12570579/how-to-get-heroku-app-name-url-from-inside-the-app) inside the app:

	heroku config:set APP_URL=$(heroku info -s | grep web_url | cut -d= -f2)

Push a branch to Heroku

	git push heroku <local_branch>:master
	

Reboot the application

	heroku restart

Links

- [Ignore files for Heroku build](	https://devcenter.heroku.com/articles/slug-compiler#ignoring-files-with-slugignore)
