# Setup Mapper: Implementation notes

## Controller

- Transforms the `request.body` with the `Deserialize` from the
  [cerialize](https://github.com/weichx/cerialize) module into an `XxxInput` object.
  Optionaly a structure transformation has to performed. The output must be an (optionally)
  filtered `XxxDocument`  data tree.
  If the request is identically to the stored data, the `XxxInput` object can be ommited.
- The `XxxInput` (or the `XxxDocument`  if the input object is ommited) is anntotaed with
  `Deserialize` and validation information. For validation the
  [class-validator](https://github.com/pleerock/class-validator) module is used.
   
  
              |
	JSON request, untyped (request.body)
              |
              V
        +------------+
        | Controller |
        +------------+
              |
	XxxInput or XxxDocument
              |
              V
        +------------+
        | Service    |
        +------------+
              |
	XxxInput or XxxDocument
              |
              V
        +------------+
        | Repository |
        +------------+

The response flow looks like

        +------------+
        | Repository |
        +------------+
              |
	     XxxDocument
              |
              V
        +------------+
        | Service    |
        +------------+
              |
	     XxxDocument
              |
              V
        +------------+
        | Controller |
        +------------+
