# Implement login/registration with Facebook OAuth: Implementation notes

## Facebook Application

An application must be registered at [Facebook Developers](https://developers.facebook.com/).
The provided app ID and app secret must be configured for the
[passport-facebook](https://github.com/jaredhanson/passport-facebook) strategy, see the config
object.

You also have to add a platform - in our case a 'web app'. For using Facebook OAuth during
development, you have to add the uri 'http://localhost:3000/'.

It does make sense to have three Facebook Applications registered, one for development, one for test
and one for production.


## OAuth in a sessionless REST api

OAuth works with client side redirects. The token return by Facebook is transfered into a JWT
token. We open a Facebook page for authentification, redirect to a callback URL and then redirect
to a success or error page. When we have sucess we handle the JWT token as an URL parameter to
the client application.


## Issues and support

- [Question: Optional substructure](https://github.com/SierraSoftworks/Iridium/issues/67)
- [Facebook redirect url issue OAuthException](http://stackoverflow.com/questions/16562602/facebook-redirect-url-issue-oauthexception)
- [Facebook development in localhost](http://stackoverflow.com/a/7493806/734687)
- Why enableProof? See [FacebookTokenError](https://github.com/jaredhanson/passport-facebook/issues/93)
- [Token based, sessionless auth using express and passport](https://jeroenpelgrims.com/token-based-sessionless-auth-using-express-and-passport)

