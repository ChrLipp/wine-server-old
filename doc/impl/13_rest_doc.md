# Implement REST interface documentation: Implementation notes

## Document with ApiDoc

The documentation is generated at build time with [ApiDoc](http://apidocjs.com).
[gulp-apidoc](https://github.com/c0b41/gulp-apidoc) is used to integrate ApiDoc in the build.

When you provide the field `sampleUrl` in `apidoc.json` a form to test an api method will be
generated. However you need to activate CORS on the server when you want to access it.

The documentation was not fully implemented, because I stopped development.


## Issues and support

- [parser plugin 'return'](https://github.com/apidoc/apidoc/issues/316)
- [Included sampleUrl in apidoc.json - getting "Error 0:" in the Response form field](https://github.com/apidoc/apidoc/issues/293)