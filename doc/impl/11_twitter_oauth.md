# Implement login/registration with Twitter OAuth: Implementation notes

## Twitter Application

An application must be registered at [Twitter Application Management](https://apps.twitter.com/).
The provided consumer key and consumer secret must be configured for the
[passport-twitter](https://github.com/jaredhanson/passport-twitter) strategy, see the config
object.

It does make sense to have three Twitter Applications registered, one for development, one for test
and one for production.

Important is that on the permission settings page the acess right is 'Read only' and the
'Additional Permissions' are 'Request email addresses from users'.

Twitter uses OAuth 1.0 and therfore requires session support. Jared Hanson (the author of passport)
recommends enabling state for OAuth 2 which also requires sessions - a temporary secret is stored
in the session to prevent cross site scripting attacks.

Sessions should use secure cookies when used with https only (in production). But Heroku terminates
SSL before it reaches the app. That causes express to see non-ssl traffic, so express can't read the
proxy unless you activate the proxy in the session support and also trust the proxy.


## Mandatory environment variables

- SESSION_SECRET for encrypting sessions
- TWITTER_CONSUMER_KEY for Twitter consumer key
- TWITTER_CONSUMER_SECRET for Twitter consumer secret


## Issues and support

- [Get user email](https://github.com/jaredhanson/passport-twitter/issues/67#issuecomment-216644224)
- [Add the 'state' query parameter to the authenticate call](https://github.com/jaredhanson/passport-facebook/issues/14#issuecomment-6193288)
- [How to set secure cookie using heroku + node.js + express?](http://stackoverflow.com/questions/14463972/how-to-set-secure-cookie-using-heroku-node-js-express)
