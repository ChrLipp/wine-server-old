# Setup Logging: Implementation notes

## Logging

A singleton instance of a [winston](https://github.com/winstonjs/winston) logger is used
from many lightweighted loggers (one instance per module).

Request (only stats) are logged via [Morgan](https://github.com/expressjs/morgan).

There are two transports, `Console` and MongoDB. First, I tried with
[winston-mongodb](https://github.com/winstonjs/winston-mongodb), but then I switched to a
self implemented solution. Logging into MongoDB is expensiv, but for Heroku it is a cheap
alternative to any commercial logging addon.

Additionaly [express-winston](https://github.com/bithavoc/express-winston) is included to log
each request/response in detail, currently on every stage (should be development environment only).


## Retrieving

Use the following command to query the MongoDB logs:

    db.getCollection('Log').find({}).sort({_id: 1})
    
Without the sort order the logs may be retrieved in wrong order.


## Open issues

- Make the logging configurable to reduce it
  (at the moment we have development logging which can't be reduced) 
