# Implement configuration management: Implementation notes

## Implementation

The implementation consists of

- `config.ts` (which defines all environment variables)
- `config.loader.ts` (which provides all utility functions)

Settings can either be supplied by defining an environment variable and/or
by providing a local `.env` file.

Libraries used

- [dotenv](https://github.com/motdotla/dotenv) for loading `.env` files
- [class-validator](https://github.com/pleerock/class-validator) already used for mapping

Other interesting libraries are
 
- [confeta](https://github.com/Deadarius/confeta)
- [nconf](https://github.com/indexzero/nconf)
- [node-convict](https://github.com/mozilla/node-convict)

 
## Issues and support

[How to handle different environments and configurations?](https://blog.risingstack.com/node-js-project-structure-tutorial-node-js-at-scale/#howtohandledifferentenvironmentsandconfigurations)