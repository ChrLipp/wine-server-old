# Setup express server: Implementation notes

## Inversify Express utils

- [Inversify Express utils](https://github.com/inversify/inversify-express-utils)
- based on [Inversify IOC Container](http://inversify.io/)


This allows to decorate controlles and use DI to wire your app.

## Nodemon 

For easy restart on changes Nodemon was included: [gulp-nodemon](https://www.npmjs.com/package/gulp-nodemon)

## Heroku

Heroku sets NPM_CONFIG_PRODUCTION to true to install production dependencies only and recommends
to move only the dependencies you actually need for production builds (gulp, etc) into dependencies.

Since gulp reports errors if not all required dependencies are available, I used lazy loading:

    var $ = require('gulp-load-plugins')({ lazy: true });
    
Only necessary packages are directly required and are installed directly in the dependencies.
All other plugins are in devDependecies, therefor not loaded in Heroku and are lazy loaded to
avoid gulp errors.

## Express

The following middleware was used:

- [compression](https://github.com/expressjs/compression) for compression of responses
- [helmet](https://github.com/helmetjs/helmet) for setting some security headers


    app.use(compression());    
    app.use(helmet());
