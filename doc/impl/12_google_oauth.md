# Implement login/registration with Google OAuth: Implementation notes

## Building a Heroku test stage

The Heroku test stage should be updated whenever a developer pushes to the corresponding git repo.
The Heroku production stage should only be updated by a manual user interaction - a promoting from
stage test to stage production.

First I have to create a test stage - a clone from the current production stage:

	heroku fork --from wine-server --to wine-server-test
 
Next, the production settings have to be changed (use the Facebook and Twitter test account, use
different secrets as in production, ...).
 
Then set the Heroku git remote url from production system to test system:
 
	git remote -v														# list your remotes
	git remote remove heroku											# remove remote heroku
	git remote add heroku https://git.heroku.com/wine-server-test.git	# add test remote

Check the Acess rights of your apps, e.g. move the devs from production to test.

Then create the pipeline:

- Open the production app in the Heroku dashboard
- Go to 'Deploy' and create a pipline
- The production app should be on the right side (in the production field)
- Add the test app on the left side
- Next, perform a test deployment first to the test stage and then to the production stage


## Debugging

Debugging (breakpoints in Typescript) does work for the application, for integration tests and
even for the Gulp build. The reason is the instrumentation by Istanbul, without instrumentation
everything is ok.
 
Solution: provide two unit/integration/all test tasks, one with and one without instrumentation. 


## Google Application

It does make sense to have three Google Applications registered, one for each stage. For the new
test stage also an additional Twitter and Facebook Application has to be registered. With the
abstract class `AbstractOAuthStrategyFactory` in place it should be straightforward to implement
a new OAuth scheme.

- install `passport-google-oauth20` with

    yarn add passport-google-oauth20

- register 3 applications at the [Google Developers Console](https://console.developers.google.com/)
  You have to activate Google+ API. For screenshots look at the "passport google oauth on localhost"
  link
- set `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_ID` in the environment
- provide access via config
- create a Google strategy factory 
- wire it up in the DI setup

To use `passport-google-oauth20` (currently no typings) you have to set

    "noImplicitAny": false,


## Mandatory environment variables

- GOOGLE_CLIENT_ID
- GOOGLE_CLIENT_SECRET


## Issues and support

- [Managing Multiple Environments for an App](https://devcenter.heroku.com/articles/multiple-environments)
- [Forking Applications](https://devcenter.heroku.com/articles/fork-app)
- [Gulp 4](https://github.com/gulpjs/gulp/tree/4.0)
- [passport google oauth on localhost](http://stackoverflow.com/questions/24352975/passport-google-oauth-on-localhost)
- [A node modul without typings](http://stackoverflow.com/questions/43034921/a-node-modul-without-typings)