# Setup express server

As a developer, I want to have a configured server so that
it is easy to develop new functionality without dealing with infrastructure code.

## Requirements

- Extend Gulp 4 with
    - nodemon
- Setup express server
- Deployment on Heroku

## Acceptance tests

- GET request to the static route on Heroku delivers static file (200 OK)
- GET request to the health check on Heroku delivers defined content for health check (200 OK)

## Implementation notes

See [here](../impl/02_express.md)