# Implement registration with username / password

As an user I can register myself in order to use the service. 

## Requirements

- Registration with username / password
- The password has to be entered twice
- The email used with the registration has to be confirmed
  by sending a mail with a confirmation link
- In case of invalid data the client receives a list of validation errors
- Temporary email providers are rejected (configuration option)


## Acceptance tests

- The password is stored as a hash in the MongoDB
- Both password fields are equal
- The email format is validated, invalid formats are rejected.
- The email is stored in lower case.
- A registration with a mailinator email adress failes. 
- Already registered users are not allowed to repeat the registration 
- After registration the state is `unconfirmed` 
- After registration and confirmation the state changes to `confirmed`
- Multiple confirmations have no implications.
- The validation errors list is delivered to the client and correspond to the entered data


## Implementation notes

See [here](../impl/08_registration.md)
