# Implement login/registration with Google OAuth

As a user I want to register / login to the service with my Google account. 


## Requirements

- User can choose Google as a registration / login method
- The Google access token is transformed in a JWT token to have a uniform access scheme
- The Google profile data are stored within the user
- If a user choosed prior a different registration method, the user data is extended with the
  Google profile information
- Diffenetiate between Heroku test and production stage, update production stage from test stage
  with pipeline


## Acceptance tests

- Registration of an user with Google OAuth is successful
- Login of a registered user with Google OAuth and
  an valid Google access token is successful
- Login of a registered user with Google OAuth and
  an invalid Google access token is successful
- When a user registered with local user/password registers him with Google OAuth (same email)
  then the user gets merged (local info and Google info are stored both)
- Common success and error page from previous OAuth implenentation are used


## Implementation notes

See [here](../impl/12_google_oauth.md)

