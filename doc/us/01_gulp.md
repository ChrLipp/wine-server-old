# Setup directory structure and build with Gulp 4

As a developer, I want to have an existing project structure and an automated build
so that I don't have manually perform tasks.

## Requirements

- Define directory structure
- Application must use Node 6.x
- Define Gulp 4 build including
    - Typescript 2.x with ES2016 async/await
    - lint
- Create git repository

## Acceptance tests

- Able to compile `let` and `async` / `await` statements

## Implementation notes

See [here](../impl/01_gulp.md)