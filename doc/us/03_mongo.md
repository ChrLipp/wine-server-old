# Setup MongoDB

As a developer, I want to have an existing MongoDB infrastructure.

## Requirements

- Sample implementation of CRUD with repository pattern
- The choosen model should be `StorageSpace`.
- The model `StorageSpace` must have the following attributes
-- id:              the MongoDb identifier
-- name:            Name of the storage space
-- lastInventory:   Timestamp of the last inventory
-- currentAmount:   Current amount of bottles in the storage space
-- maximalAmount:   Maximal amount of bottles which have ever been in the storage space

## Acceptance tests

- Create a storage space via REST
- Retrieve the storage space via REST
- Update the storage space via REST
- Delete the storage space via REST

## Restrictions

- Storage space is dependend on user id. At the current time this information is not available.
Therefore only a limited model is choosen for the sake of setting the focus to the MongoDB operations.
- The model used for requests / responses is the same as the model stored in the collections.
A mapper user story will follow.   


## Implementation notes

See [here](../impl/03_mongo.md)