# Implement REST interface documentation

As a client programmer I want to use only documented services. 


## Requirements

- Provide a HTML documentation of every REST service


## Acceptance tests

- Documentation is available
