# Setup Mapper

As a developer I can distinguish between the
- external (API request/response) and
- internal (storage)

data model.

## Requirements

- Provide automatic mapping
- Allow to manually fine tune the mapping
- Validate the external model
- Remve additional, not wanted attributes
 
## Acceptance tests

- Requests are only allowed to modify the attribute 'name' of the storage space.
- No MongoDB error message is transported to the client.
- Invalid data is rejected with http status code 400.


## Implementation notes

See [here](../impl/05_mapper.md)
