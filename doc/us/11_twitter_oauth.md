# Implement login/registration with Twitter OAuth

As a user I want to register / login to the service with my Twitter account. 


## Requirements

- User can choose Twitter as a registration / login method
- The Twitter access token is transformed in a JWT token to have a uniform access scheme
- The Twitter profile data are stored within the user
- If a user choosed prior a different registration method, the user data is extended with the
  Twitter profile information


## Acceptance tests

- Registration of an user with Twitter OAuth is successful
- Login of a registered user with Twitter OAuth and
  an valid Twitter access token is successful
- Login of a registered user with Twitter OAuth and
  an invalid Twitter access token is successful
- When a user registered with local user/password registers him with Twitter OAuth (same email)
  then the user gets merged (local info and Twitter info are stored both)
- Success and error page from Facebook OAuth are used


## Implementation notes

See [here](../impl/11_twitter_oauth.md)

