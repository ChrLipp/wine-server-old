# Setup test infrastructure

As a developer I have an existing test infrastructure.

## Requirements

- Provide an unit test infrastructure
	- Provide a test framework
	- Provide an optional assertion library
	- Provide a framework for mocking/stubbing/spying
	- Integrate in build
	- Provide test coverage
	- Integrate in npm
- Provide an integration test infrastructure 

## Acceptance tests

- `gulp test` executes unit and integration tests and displays the test coverage for unit tests
- `npm test` calls `gulp test`
- implemented at least one unit test and one integration test

## Implementation notes

See [here](../impl/06_test.md)
