# Implement login with username / password

As a registered user I can login in order to use the service. 

## Requirements

- User can login to the system using a name and password
- No session is used
- The user receives a Json Web Token (JWT) as the response of the login


## Acceptance tests

- Correct login results in delivery of a JWT
- Incorrect login results in an 401 error (Unauthorized)
- The lastLogin timestamp for a user is updated
- The storage space ressource is saved with the corresponding user
- Manually check the JWT at [jwt.io](http://jwt.io)

## Implementation notes

See [here](../impl/09_login.md)

