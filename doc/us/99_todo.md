Implement / integrate

- Test of Oauth
- Markdown [HTML](https://github.com/sindresorhus/gulp-markdown) und [PDF](https://github.com/sindresorhus/gulp-markdown-pdf)
- nodemon
- Incremental compilation
- Document API
- Fehlermeldung Mail sending übersetzen
- when typings for `passport-google-oauth20` are available,turn on `noImplicitAny`
