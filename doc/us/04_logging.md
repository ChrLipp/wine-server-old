# Setup Logging

As a developer I have customizeable logging so that I am able to retrieve maximal
information about was going on inside my application.

## Requirements

- Provide asynchron logging
- Remove all `console.log` calls
- Logging of requests and responses during development mode (locally)
 
## Acceptance tests

- linter rule for `console.log` usage


## Implementation notes

See [here](../impl/04_logging.md)
