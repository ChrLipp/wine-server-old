# Implement login/registration with Facebook OAuth

As a user I want to register / login to the service with my Facebook account. 


## Requirements

- User can choose Facebook as a registration / login method
- The Facebook access token is transformed in a JWT token to have a uniform access scheme
- The Facebook profile data are stored within the user
- If a user choosed prior a different registration method, the user data is extended with the
  Facebook profile information


## Acceptance tests

- Registration of an user with Facebook OAuth is successful
- Login of a registered user with Facebook OAuth and
  an valid Facebook access token is successful
- Login of a registered user with Facebook OAuth and
  an invalid Facebook access token is successful
- When a user registered with local user/password registers him with Facebook OAuth (same email)
  then the user gets merged (local info and Facebook info are stored both)
- Success and error page are configurable


## Implementation notes

See [here](../impl/10_fb_oauth.md)

