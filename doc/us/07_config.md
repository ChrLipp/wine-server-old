# Implement configuration management

As an operator I can maintain stage specific configuration outside the application.

## Requirements

- Config the application according to [The Twelve-Factor App ](http://12factor.net/config) methodology.
- Guarantee that account details used during development are not pushed to source control.

## Acceptance tests

- Centralized config setup according to the requirements.
- Different config settings on Heroku and local
- Config PORT and validate that PORT entry is greather than 1024

## Implementation notes

See [here](../impl/07_config.md)
