module.exports = function (config) {

    return {
        files: [
            'source/**/*.ts',
            '!source/**/*.spec.ts'
        ],

        tests: [
           'source/**/*.spec.ts'
        ],

        testRunner: 'mocha',
        env: {
            type: 'node',
            runner: 'node'
        },
        debug: true,
        compilers: {
            'source/**/*.ts': config.compilers.typeScript({
                typescript: require('typescript')
            })
        },
    }
}
