# Wine server

## What is it?

This is a node.js express web server with MongoDB persistance starter written in Typescript.
It includes user registration and login, OAuth and some other features. The main reason to develop
this was to test node.js object oriented development with Typescript and modern language features
(aka async / await).

However, I stopped the development and will reimplement the functionality based on the framework
[nest](http://nestjs.com). Therefore use this repository as a reference, but not for active projects.

## Install

- `npm install -g gulp-cli` (Build tool Gulp 4.x, Gulp 3.x must be removed prior)
- `npm install -g yarn`
- `yarn`

## Run

- Start Mongo `./bin/startmongo`
- Run application `node build/source/main.js` 

## Documentation

- Directory of [user stories](doc/us/index.md)
- Directory of [implementation notes](doc/impl/index.md) (1:1 to user stories)