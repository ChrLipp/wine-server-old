'use strict';

var gulp            = require('gulp'),
    del             = require('del'),
    fs              = require('fs'),
    typescript      = require('gulp-typescript'),
    sourcemaps      = require('gulp-sourcemaps'),
    $               = require('gulp-load-plugins')({ lazy: true }),
    remapIstanbul   = require('remap-istanbul/lib/gulpRemapIstanbul'),
    config          = require('./gulp.config')();

var originalEnv = process.env.NODE_ENV;

/*******************************************************************************/
/* For the following tasks the plugins are directly required and are installed */
/* via package.json dependencies, since Heroku only installs these             */
/*******************************************************************************/

// clean build directory
gulp.task('clean', function doClean() {
    return del([
        config.paths.outDir + '**/*'
    ]);
});

// copy ressources
gulp.task('copyRessources', gulp.parallel(
    function doCopyHbs() {
        return gulp
            .src('./source/**/*.hbs')
            .pipe(gulp.dest(config.paths.outSourceDir));
    })
);

// compile typescript
gulp.task('compile', gulp.parallel('copyRessources', function doBuildApp() {
    var tsProject = typescript.createProject('tsconfig.json');
    return gulp
        .src([config.paths.sourcePattern])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('.', {
            sourceRoot: config.paths.sourceDir
        }))
        .pipe(gulp.dest(config.paths.outSourceDir));
}));

/*******************************************************************************/
/* All other plugins are in devDependecies, therefor not loaded in Heroku and  */
/* are lazy loaded in the following tasks to avoid gulp errors                 */
/* The only exception is remapIstanbul, since this module doesn't follow the   */
/* gulp-<name> pattern, which is required by gulp-load-plugins                 */
/*******************************************************************************/

// lint for code in source and test
gulp.task('lint', function doLint() {
    var tslint = require("tslint");
    var program = tslint.Linter.createProgram("./tsconfig.json");

    return gulp
        .src([
            config.paths.sourcePattern
        ])
        .pipe($.tslint({
            formatter:  "verbose",
            program:    program
        }))
        .pipe($.tslint.report());
});

// build for compiling and linting
gulp.task('build', gulp.series('compile', 'lint'));

// API Documentation
gulp.task('apidoc', function(done) {
    $.apidoc({
        src: config.paths.sourceDir,
        dest: config.paths.outApiDocDir
    },done);
});

gulp.task('serve-apidoc', gulp.series('apidoc', function copyApiDoc() {
    return gulp
        .src('./build/apidoc/**/*')
        .pipe(gulp.dest('./public'));
}));

// Start instrumentation, necessary for measuring code coverage.
function startInstrumentation() {
    process.env.NODE_ENV = 'test';
    return gulp
        .src([config.paths.outSourcePattern])
        .pipe($.istanbul())
        .pipe($.istanbul.hookRequire());
}

// Runs the unit test.
function doUnitTest() {
    return gulp
        .src(config.paths.outUnitTestPattern)
        .pipe($.mocha({
            ui: 'bdd',
            timeout: 30000
        }));
}

// Runs the unit test and writes an instrumentation report.
function doUnitTestWithInstrumentation() {
    return doUnitTest()
        .pipe($.istanbul.writeReports({
            dir: config.paths.outCoverageDir + 'js-json',
            reporters: ['json'],
        }));
}

// Generates the coverage reports.
function endInstrumentation() {
    process.env.NODE_ENV = originalEnv;
    return gulp
        .src(config.paths.outCoverageDir + 'js-json/coverage-final.json')
        .pipe(remapIstanbul({
            reports: {
                'json': config.paths.outCoverageDir + 'ts-json/coverage.json',
                'html': config.paths.outCoverageDir + 'html-report',
                'text-summary': ''
            }
        }));
}

// Runs the unit test with instrumentation.
function unitTestsWithCoverage() {
    return gulp.series(startInstrumentation, doUnitTestWithInstrumentation, endInstrumentation);
}

// runs the integration test
function doIntegrationTest() {
    process.env.NODE_ENV = 'test';
    return gulp
        .src(config.paths.outIntegrationTestPattern)
        .pipe($.mocha({
            ui: 'bdd'
        }));
}

// Runs the integration test and writes an instrumentation report.
function doIntegrationTestWithInstrumentation() {
    return doIntegrationTest()
        .pipe($.istanbul.writeReports({
            dir: config.paths.outCoverageDir + 'js-json',
            reporters: ['json'],
        }));
}

// ends the integration test
function doPostIntegrationTest(done) {
    process.env.NODE_ENV = originalEnv;
    done();
}

// runs the integration test with instrumentation.
function integrationTestsWithCoverage() {
    return gulp.series(startInstrumentation,
        doIntegrationTestWithInstrumentation, endInstrumentation);
}

// tasks for all combinations
gulp.task('unit-test', gulp.series('compile', unitTestsWithCoverage()));
gulp.task('unit-test-no-coverage', gulp.series('compile', doUnitTest));
gulp.task('integration-test', gulp.series('compile', integrationTestsWithCoverage()));
gulp.task('integration-test-no-coverage', gulp.series('compile',
    doIntegrationTest, doPostIntegrationTest));
gulp.task('test', gulp.series('compile',  startInstrumentation,
    doUnitTestWithInstrumentation, doIntegrationTestWithInstrumentation, endInstrumentation));
gulp.task('test-no-coverage', gulp.series('compile',
    doUnitTest, doIntegrationTest, doPostIntegrationTest));

// run and restart
gulp.task('serve', gulp.series('compile', function doServe() {
    $.nodemon(config.nodemon);
}));

gulp.task('serve-debug', gulp.series('compile', function doServeDebug() {
    config.nodemon.exec = 'node --debug';
    $.nodemon(config.nodemon);
}));

// watcher
gulp.task('watch', gulp.series('compile', function doWatch() {
    gulp.watch([
        config.paths.sourcePattern
    ],
    function executeWatch(cb) {
        gulp.series('compile')(function wrappedCb(err) {
            cb();
        });
    })
}));

// define default task
gulp.task('default', gulp.series('clean', 'build'));
