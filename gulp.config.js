module.exports = function() {
    var sourceDir   = 'source/';
    var outDir      = 'build/';

    var outSourceDir = outDir + sourceDir;

    var config = {
        paths : {
            sourceDir:                  sourceDir,
            sourcePattern:              sourceDir + '**/*.ts',
            outDir:                     outDir,
            outSourceDir:               outSourceDir,
            outSourcePattern:           outSourceDir + '**/*.js',
            outApiDocDir:               outDir + 'apidoc/',
            outIntegrationTestPattern:  outSourceDir + '**/*.ispec.js',
            outUnitTestPattern:         outSourceDir + '**/*.spec.js',
            outCoverageDir:             outDir + 'coverage/'
        },
        nodemon: {
            script: outSourceDir + 'main.js',
            ext: 'js',
            watch: [outSourceDir],
            exec: 'node',
            env: {
                'NODE_ENV': 'development'
            }
        }

    };
    
    return config;
};